/*
 * Copyright (c) 2006-2011, dhiller, http://www.dhiller.de
 * Daniel Hiller, Warendorfer Str. 47, 48231 Warendorf, NRW, Germany, 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 * 
 * - Redistributions of source code must retain the above copyright notice, this 
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, 
 *   this list of conditions and the following disclaimer in the documentation 
 *   and/or other materials provided with the distribution.
 * - Neither the name of dhiller nor the names of its
 *   contributors may  
 *   be used to endorse or promote products derived from this software without 
 *   specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.dhiller.menuorders;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import de.dhiller.database.DBField;
import de.dhiller.database.DBManager.DBManagerException;
import de.dhiller.database.DBObject;
import de.dhiller.database.DBProperty;


public class DailyTotalByMenuWithPrice extends DBObject {

  // Declarations
  // ==================================================================================================================

  // Class ( public , package-private , protected , private - final first )
  // ----------------------------------------------------------------------------------------------
  public static final DBField< Date >               DELIVERY_DATE = new DBField< Date >(
                                                                    "DeliveryDate" , "Liefertermin" , Date.class , false, 0 );
  public static final DBField< String >     ARTICLE_NAME  = new DBField< String >(
	    "ArticleName", "Menü", String.class, false, 1);
  public static final DBField< Integer >    TOTAL_AMOUNT  = new DBField< Integer >(
                                                                    "TotalAmount" , "Anzahl bestellt gesamt" , Integer.class , false, 2 );
  public static final DBField< BigDecimal > PRICE         = new DBField< BigDecimal >(
                                                                    "Price" , "Einzelpreis" , BigDecimal.class , false, 3 );
  public static final DBField< BigDecimal > TOTAL_PRICE   = new DBField< BigDecimal >(
                                                                    "TotalPrice" , "Gesamtpreis" , BigDecimal.class , false, 4 );
  public static final DBField< Boolean >    PLACED        = new DBField< Boolean >(
                                                                    "Placed" , "Bestellt" , Boolean.class , false, 5 );

  private static final List< DBField< ? >>  FIELDS;

  // Instance
  // ----------------------------------------------------------------------------------------------


  // Inner classes
  // ==================================================================================================================

  // Public
  // ----------------------------------------------------------------------------------------------


  // Constructors
  // ==================================================================================================================
  
  // Static
  // ----------------------------------------------------------------------------------------------
  
  static {
    List< DBField< ? >> l = new ArrayList< DBField< ? > >();
    l.add( DELIVERY_DATE );
    l.add( ARTICLE_NAME );
    l.add( TOTAL_AMOUNT );
    l.add( PRICE );
    l.add( TOTAL_PRICE );
    l.add( PLACED );
    FIELDS = Collections.unmodifiableList( l );
  }
  

  // Public
  // ----------------------------------------------------------------------------------------------
  
  /**
   * Erzeugt eine neue <code>DailyTotalByMenuWithPrice</code>-Instanz.
   *
   */
  public DailyTotalByMenuWithPrice() {
    super( "DailyTotalsByMenuWithPrice" );
  }
  
  
  protected DailyTotalByMenuWithPrice( Set< DBProperty< ? > > properties ) {
    super( "DailyTotalsByMenuWithPrice" );
    setProperties( properties );
  }


  // Class methods
  // ==================================================================================================================

  // Public
  // ----------------------------------------------------------------------------------------------

  /**
   * Gibt die {@link DailyTotalByMenuWithPrice}-Instanzen zurück.
   * 
   * @return die {@link DailyTotalByMenuWithPrice}-Instanzen
   * @throws DBManagerException
   */
  public static List< DailyTotalByMenuWithPrice > getInstances( ) throws DBManagerException {
    final ArrayList< DailyTotalByMenuWithPrice > result = new ArrayList< DailyTotalByMenuWithPrice >();
    final List< Set< DBProperty< ? >>> propertySets = MenuOrders.getDBManager().getPropertySets(
      new DailyTotalByMenuWithPrice() , null );
    for ( Set< DBProperty< ? > > objProps : propertySets ) {
      result.add( new DailyTotalByMenuWithPrice( objProps ) );
    }
    return result;
  }


  // Instance methods
  // ==================================================================================================================

  // Public
  // ----------------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   *
   * @return {@inheritDoc}
   *
   * @see de.dhiller.database.DBObject#getFields()
   */
  @Override
  public List< DBField< ? >> getFields() {
    return FIELDS;
  }

}
