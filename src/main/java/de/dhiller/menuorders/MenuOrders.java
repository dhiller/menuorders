/*
 * Copyright (c) 2006-2011, dhiller, http://www.dhiller.de Daniel Hiller, Warendorfer Str. 47, 48231 Warendorf, NRW,
 * Germany, All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 * 
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and the following
 * disclaimer. - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution. - Neither the
 * name of dhiller nor the names of its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.dhiller.menuorders;

import static de.dhiller.menuorders.MenuOrders.ComponentNames.*;
import static de.dhiller.menuorders.State.BUSY;
import static de.dhiller.menuorders.State.IDLE;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.print.PrinterException;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.prefs.Preferences;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import org.jdesktop.swingx.JXTable;
//import javax.swing.JTable.DateRenderer;
import org.jdesktop.swingx.JXMonthView;
import org.jdesktop.swingx.calendar.DateSelectionModel.SelectionMode;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.Highlighter;
import org.jdesktop.swingx.table.TableColumnExt;
import javax.swing.SwingWorker;

import de.dhiller.database.DBField;
import de.dhiller.database.DBManager;
import de.dhiller.database.DBManager.ConnectionProperties;
import de.dhiller.database.DBManager.DBManagerException;
import de.dhiller.database.DBObject;
import de.dhiller.database.DBProperty;
import de.dhiller.util.Test;

/**
 * Die Hauptanwendung.
 * 
 * @author dhiller (creator)
 * @since 09.09.2006 16:01:44
 */
public class MenuOrders extends JFrame {

    static class ComponentNames {

	static final String TABBED_PANE = "tabbedPane";
	static final String FRAME = "MenuOrders";
	static final String MONTH_VIEW = "monthView";
	static final String PERSON_LIST = "list_Persons";
	static final String ORDERS_FOR_ONE_PERSON_AND_WEEK = "panel_OrdersForOnePersonAndWeek";
	static final String ALL_ORDERS_FOR_WEEK = "panel_AllOrdersForWeek";
	static final String TOGGLE_PAID = "panel_MarkPaidUnpaid";
	static final String ORDERS_BY_PERSON_AND_DAY = "panel_OrdersByPersonAndDay";
	static final String ORDERS_BY_PERSON_PER_MONTH = "panelOrdersByPersonPerMonth";
	static final String MONTH_SPINNER = "monthSpinner";
	static final String YEAR_SPINNER = "yearSpinner";
	static final String TOGGLE_ORDER_PLACED = "checkBox_PlacedOrder";
	static final String ALL_ORDERS_FOR_WEEK_TABLE = "table_AllOrdersForWeek";
	static final String ALL_ORDERS_TOTAL = "label_AllOrdersTotal";
	static final String ALL_ORDERS_PAID = "label_AllOrdersPaid";
	static final String ALL_ORDERS_REMAINING = "label_AllOrdersRemaining";
	static final String ORDERS_FOR_ONE_PERSON_AND_WEEK_TABLE = "table_MyOrdersForWeek";
	static final String TOGGLE_PAID_UNPAID = "checkBox_MarkPaidUnpaid";
	static final String CREATE_NEW_ORDER = "button_CreateNewOrder";
	static final String ORDERS_BY_PERSON_AND_DAY_TABLE = "table_OrdersByPersonAndDay";
	static final String PRINT_BUTTON = "button_Print";

    }

    /**
     * Das Datumsformat fuer die Datenbank.
     */
    public static final SimpleDateFormat DATEFORMAT = new SimpleDateFormat(
	    "yyyy-MM-dd");

    private static final long serialVersionUID = -8338303670692153040L;

    private static final Color TODAY_COLOR = new Color(0xFE, 0xFF, 0X8F); // new
									  // Color(
									  // 0xC0,
									  // 0xFF,
									  // 0X80
									  // );

    // Instance
    // ----------------------------------------------------------------------------------------------

    private DBManager dbManager;
    private final Map<Person, BigDecimal> ordereds = new Hashtable<Person, BigDecimal>();
    private final Map<Person, BigDecimal> paids = new Hashtable<Person, BigDecimal>();

    private Vector<Vector> vector_AllOrdersForWeek;
    private Vector<Vector> vector_MyOrdersForWeek;
    private Vector<Vector> vector_OrdersByPersonAndDay;
    private DBObjectTableModel allOrdersForWeekModel;
    private JXMonthView monthView;
    private JLabel label_AllOrdersTotal;
    private JLabel label_AllOrdersPaid;
    private JLabel label_AllOrdersRemaining;
    private DBObjectTableModel myOrdersForWeekModel;
    private Person[] persons;
    private JList list_Persons;
    private JXTable table_MyOrdersForWeek;
    private JCheckBox checkBox_MarkPaidUnpaid;
    private ActionListener setPaidUnpaidActionListener;
    private JButton button_CreateNewOrder;
    private JCheckBox checkBox_PlacedOrder;
    private JXTable table_AllOrdersForWeek;

    private ActionListener checkBox_PlacedOrder_ActionListener;

    private State state = BUSY;

    private DBObjectTableModel model_OrdersByPersonAndDay;

    private JXTable table_OrdersByPersonAndDay;

    public static final String FILE_NAME = "hsqldb";

    private static DBManager dbManagerInstance;

    private final BigDecimalRenderer bigDecimalRenderer = new BigDecimalRenderer();

    private final class SelectRowAndColumnOnRightClickMouseAdapter extends
	    MouseAdapter {

	@Override
	public void mousePressed(MouseEvent arg0) {
	    if (arg0.getButton() != MouseEvent.BUTTON3)
		return;
	    final int row = table_MyOrdersForWeek.rowAtPoint(arg0.getPoint());
	    table_MyOrdersForWeek.getSelectionModel().setSelectionInterval(row,
		    row);
	    final int column = table_MyOrdersForWeek.columnAtPoint(arg0
		    .getPoint());
	    table_MyOrdersForWeek.getColumnModel().getSelectionModel()
		    .setSelectionInterval(column, column);
	}
    }

    // Inner classes
    // ==================================================================================================================

    // Public
    // ----------------------------------------------------------------------------------------------

    private final class ExportOrdersByPersonPerMonth extends AbstractAction {

	private final JPanel panelOrdersByPersonPerMonth;
	String ordersByPersonPerMonthAsCSV = "";

	private ExportOrdersByPersonPerMonth(String arg0,
		JPanel panelOrdersByPersonPerMonth) {
	    super(arg0);
	    this.panelOrdersByPersonPerMonth = panelOrdersByPersonPerMonth;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
	    if (ordersByPersonPerMonthAsCSV.isEmpty()) {
		JOptionPane.showConfirmDialog(MenuOrders.this,
			"Export enthält keine Daten!?");
		return;
	    }
	    final JFileChooser jFileChooser = new JFileChooser(new File(
		    System.getProperty("user.home"),
		    "OrdersByPersonPerMonth.csv"));
	    jFileChooser.showSaveDialog(MenuOrders.this);
	    if (jFileChooser.getSelectedFile() == null)
		return;
	    if (jFileChooser.getSelectedFile().exists()
		    && JOptionPane.showConfirmDialog(MenuOrders.this,
			    "Datei existiert bereits. Überschreiben?",
			    "Datei überschreiben bestätigen",
			    JOptionPane.OK_CANCEL_OPTION) != JOptionPane.OK_OPTION)
		return;
	    new Thread() {

		public void run() {
		    try {
			final BufferedWriter bufferedWriter = new BufferedWriter(
				new FileWriter(jFileChooser.getSelectedFile()));
			try {
			    bufferedWriter.write(ordersByPersonPerMonthAsCSV);
			} finally {
			    bufferedWriter.close();
			}
		    } catch (IOException e) {
			JOptionPane.showMessageDialog(
				MenuOrders.this,
				"Datei schreiben fehlgeschlagen!\n\n"
					+ e.getMessage());
			e.printStackTrace();
		    }
		}
	    }.start();
	}
    }

    private final class OrdersByPersonPerMonthUpdater extends
	    SwingWorker<Vector<Vector<Object>>, Object> {

	private final NumberFormat decimalFormat = DecimalFormat
		.getInstance(Locale.GERMAN);
	private final JXTable xTableOrdersByPersonPerMonth;
	private final JSpinner yearSpinner;
	private final JSpinner monthSpinner;
	final Integer month;
	final Integer year;
	final ExportOrdersByPersonPerMonth export;

	private OrdersByPersonPerMonthUpdater(
		JXTable xTableOrdersByPersonPerMonth, JSpinner yearSpinner,
		JSpinner monthSpinner, ExportOrdersByPersonPerMonth export) {
	    this.xTableOrdersByPersonPerMonth = xTableOrdersByPersonPerMonth;
	    this.yearSpinner = yearSpinner;
	    this.monthSpinner = monthSpinner;
	    month = (Integer) monthSpinner.getValue();
	    year = (Integer) yearSpinner.getValue();
	    this.export = export;
	    decimalFormat.setGroupingUsed(false);
	    decimalFormat.setMinimumFractionDigits(2);
	    decimalFormat.setMaximumFractionDigits(2);
	}

	protected Vector<Vector<Object>> doInBackground() throws Exception {
	    setStateBusy();
	    String sql = "select PERSONNAME, DELIVERYDATE, ARTICLENAME, AMOUNT, PRICE, TOTALPRICE "
		    + "from ordercostperpersonandday "
		    + "where year(DELIVERYDATE) = {0,number,#} and month(DELIVERYDATE) = {1} "
		    + "order by PERSONNAME, DELIVERYDATE;";
	    final String selectQuery = MessageFormat.format(sql, year, month);
	    final ResultSet openRS = MenuOrders.getDBManager().getConnection()
		    .createStatement().executeQuery(selectQuery);
	    final Vector<Vector<Object>> data = new Vector<Vector<Object>>();
	    final StringBuilder csvBuilder = new StringBuilder();
	    final List<String> columnNames = Arrays.asList("PERSONNAME",
		    "DELIVERYDATE", "ARTICLENAME", "AMOUNT", "PRICE",
		    "TOTALPRICE");
	    appendRow(csvBuilder, columnNames);
	    while (openRS.next()) {
		final int columnIndex = 5;
		final List<Object> row = Arrays.<Object> asList(
			openRS.getString(1), openRS.getDate(2),
			openRS.getString(3), openRS.getInt(4),
			formatBigDecimal(openRS, columnIndex),
			formatBigDecimal(openRS, 6));
		data.add(new Vector(row));
		appendRow(csvBuilder, row);
	    }
	    export.ordersByPersonPerMonthAsCSV = csvBuilder.toString();
	    return data;
	}

	protected void appendRow(final StringBuilder csvBuilder,
		final List<?> values) {
	    for (int index = 0, size = values.size(); index < size; index++) {
		if (index > 0)
		    csvBuilder.append(";");
		csvBuilder.append("\"" + values.get(index) + "\"");
	    }
	    csvBuilder.append("\r\n");
	}

	private String formatBigDecimal(final ResultSet openRS,
		final int columnIndex) throws SQLException {
	    final BigDecimal bigDecimal = openRS.getBigDecimal(columnIndex);
	    return decimalFormat.format(bigDecimal.doubleValue());
	}

	protected void done() {
	    try {
		Vector<Vector<Object>> data = get();
		xTableOrdersByPersonPerMonth.setModel(new DefaultTableModel(
			data, new Vector(Arrays.asList("PERSONNAME",
				"DELIVERYDATE", "ARTICLENAME", "AMOUNT",
				"PRICE", "TOTALPRICE"))));
	    } catch (Exception ex) {
		ex.printStackTrace();
		if (ex instanceof java.lang.InterruptedException)
		    return;
	    } finally {
		setStateIdle();
	    }
	}
    }

    class DBObjectTableModel extends AbstractTableModel {

	private static final long serialVersionUID = -8128589744221681264L;

	private final DBObject protoType;
	private final Vector data;

	DBObjectTableModel(DBObject prototype, Vector data) {
	    this.protoType = prototype;
	    this.data = data;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @return {@inheritDoc}
	 * 
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	public int getColumnCount() {
	    return protoType.getFields().size();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @return {@inheritDoc}
	 * 
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	public int getRowCount() {
	    return data.size();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param rowIndex
	 * @param columnIndex
	 * @return {@inheritDoc}
	 * 
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	public Object getValueAt(int rowIndex, int columnIndex) {
	    if (data.size() <= rowIndex)
		return null;
	    final Vector vector = (Vector) data.get(rowIndex);
	    if (vector.size() <= columnIndex)
		return null;
	    return ((DBProperty) (vector).get(columnIndex)).getValue();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param columnIndex
	 * @return {@inheritDoc}
	 * 
	 * @see javax.swing.table.AbstractTableModel#getColumnClass(int)
	 */
	@Override
	public Class<?> getColumnClass(int columnIndex) {
	    return protoType.getFields().get(columnIndex).getType();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param column
	 * @return {@inheritDoc}
	 * 
	 * @see javax.swing.table.AbstractTableModel#getColumnName(int)
	 */
	@Override
	public String getColumnName(int column) {
	    return protoType.getFields().get(column).getLabel();
	}

    }

    class PersonTotalsPerWeekGetter extends SwingWorker<Object, Object> {

	protected Vector doInBackground() throws Exception {
	    setStateBusy();

	    SortedSet<Date> weekSelection = getWeekSelection();

	    Map<Integer, BigDecimal> orderedsByPersonID = new Hashtable<Integer, BigDecimal>();
	    Map<Integer, BigDecimal> paidsByPersonID = new Hashtable<Integer, BigDecimal>();

	    String sql = "SELECT PersonID , SUM(TotalPrice) "
		    + "FROM OrderCostPerPersonAndDay "
		    + "WHERE DeliveryDate BETWEEN '"
		    + DATEFORMAT.format(weekSelection.first()) + "' AND '"
		    + DATEFORMAT.format(weekSelection.last())
		    + "' GROUP BY PersonID;";
	    final ResultSet openRS = MenuOrders.getDBManager().getConnection()
		    .createStatement().executeQuery(sql);
	    while (openRS.next()) {
		Integer personID = openRS.getInt(1);
		BigDecimal ordered = openRS.getBigDecimal(2);
		if (ordered == null) {
		    ordered = new BigDecimal(0.0);
		}
		orderedsByPersonID.put(personID, ordered);
	    }

	    sql = "SELECT PersonID , SUM(TotalPrice) "
		    + "FROM OrderCostPerPersonAndDay "
		    + "WHERE DeliveryDate BETWEEN '"
		    + DATEFORMAT.format(weekSelection.first()) + "' AND '"
		    + DATEFORMAT.format(weekSelection.last())
		    + "' AND Paid = true GROUP BY PersonID;";
	    final ResultSet paidRS = MenuOrders.getDBManager().getConnection()
		    .createStatement().executeQuery(sql);
	    while (paidRS.next()) {
		Integer personID = paidRS.getInt(1);
		BigDecimal paid = paidRS.getBigDecimal(2);
		if (paid == null) {
		    paid = new BigDecimal(0.0);
		}
		paidsByPersonID.put(personID, paid);
	    }

	    ordereds.clear();
	    paids.clear();

	    for (Person p : persons) {

		ordereds.put(
			p,
			orderedsByPersonID.containsKey(p.getIdentityProperty()
				.getValue()) ? orderedsByPersonID.get(p
				.getIdentityProperty().getValue())
				: new BigDecimal(0.0));

		paids.put(
			p,
			paidsByPersonID.containsKey(p.getIdentityProperty()
				.getValue()) ? paidsByPersonID.get(p
				.getIdentityProperty().getValue())
				: new BigDecimal(0.0));

	    }

	    return null;
	}

	@Override
	protected void done() {
	    try {
		super.done();
		list_Persons.invalidate();
		list_Persons.revalidate();
		list_Persons.repaint();
	    } catch (Exception ex) {
		ex.printStackTrace();
		if (ex instanceof java.lang.InterruptedException)
		    return;
	    } finally {
		setStateIdle();
	    }
	}

    }

    class MyOrdersGetter extends SwingWorker<Vector, Object> {

	BigDecimal unpaid = new BigDecimal(0.0);
	private Boolean paid;

	protected Vector doInBackground() throws Exception {
	    setStateBusy();

	    vector_MyOrdersForWeek.clear();
	    final Person person = (Person) list_Persons.getSelectedValue();
	    if (person == null)
		return null;
	    final SortedSet<Date> weekSelection = getWeekSelection();
	    final List<OrderCostPerPersonAndDay> instances = OrderCostPerPersonAndDay
		    .getInstances();
	    paid = null;
	    for (OrderCostPerPersonAndDay d : instances) {
		final DBProperty<Date> property = d
			.getProperty(OrderCostPerPersonAndDay.DELIVERY_DATE);
		if (property.getValue().compareTo(weekSelection.first()) < 0
			|| property.getValue().compareTo(weekSelection.last()) > 0)
		    continue;
		final DBProperty<Integer> personID = d
			.getProperty(OrderCostPerPersonAndDay.PERSON_ID);
		if (!personID.getValue().equals(
			person.getIdentityProperty().getValue()))
		    continue;
		vector_MyOrdersForWeek.add(new Vector<DBProperty>(d
			.getProperties()));
		paid = (Boolean) d.getProperty(OrderCostPerPersonAndDay.PAID)
			.getValue();
	    }

	    final String sql = "SELECT SUM(TotalPrice) "
		    + "FROM OrderCostPerPersonAndDay "
		    + "WHERE DeliveryDate BETWEEN '"
		    + DATEFORMAT.format(weekSelection.first()) + "' AND '"
		    + DATEFORMAT.format(weekSelection.last())
		    + "' AND Paid = 0 AND PersonID = "
		    + person.getIdentityProperty().getValue() + ";";
	    final ResultSet openRS = MenuOrders.getDBManager().getConnection()
		    .createStatement().executeQuery(sql);
	    if (openRS.next()) {
		unpaid = openRS.getBigDecimal(1);
	    }
	    if (unpaid == null) {
		unpaid = new BigDecimal(0.0);
	    }

	    return null;
	}

	protected void done() {
	    try {
		get();
		checkBox_MarkPaidUnpaid.setEnabled(paid != null);
		checkBox_MarkPaidUnpaid
			.removeActionListener(setPaidUnpaidActionListener);
		checkBox_MarkPaidUnpaid.setSelected(paid != null && paid);
		checkBox_MarkPaidUnpaid
			.addActionListener(setPaidUnpaidActionListener);
		button_CreateNewOrder.setAction(new CreateOrModifyOrderAction(
			true));
		myOrdersForWeekModel.fireTableDataChanged();
		// table_MyOrdersForWeek.clearChangedCells();
	    } catch (Exception ex) {
		ex.printStackTrace();
		if (ex instanceof java.lang.InterruptedException)
		    return;
	    } finally {
		setStateIdle();
	    }
	}
    }

    class AllOrdersGetter extends SwingWorker<Vector, Object> {

	BigDecimal total = new BigDecimal(0.0);
	BigDecimal paid = new BigDecimal(0.0);
	Boolean placed = null;

	protected Vector doInBackground() throws Exception {
	    setStateBusy();
	    final SortedSet<Date> weekSelection = getWeekSelection();
	    vector_AllOrdersForWeek.clear();
	    final List<DailyTotalByMenuWithPrice> instances = DailyTotalByMenuWithPrice
		    .getInstances();
	    for (DailyTotalByMenuWithPrice d : instances) {
		final DBProperty<Date> property = d
			.getProperty(DailyTotalByMenuWithPrice.DELIVERY_DATE);
		if (property.getValue().compareTo(weekSelection.first()) < 0
			|| property.getValue().compareTo(weekSelection.last()) > 0)
		    continue;
		final DBProperty<BigDecimal> totalPrice = d
			.getProperty(DailyTotalByMenuWithPrice.TOTAL_PRICE);
		if (total == null) {
		    total = totalPrice.getValue();
		} else {
		    total = total.add(totalPrice.getValue());
		}
		vector_AllOrdersForWeek.add(new Vector(d.getProperties()));

		placed = (Boolean) d.getProperty(
			DailyTotalByMenuWithPrice.PLACED).getValue();
	    }

	    final String sql = "SELECT SUM(TotalPrice) "
		    + "FROM OrderCostPerPersonAndDay "
		    + "WHERE DeliveryDate BETWEEN '"
		    + DATEFORMAT.format(weekSelection.first()) + "' AND '"
		    + DATEFORMAT.format(weekSelection.last())
		    + "' AND Paid = 1;";
	    final ResultSet openRS = MenuOrders.getDBManager().getConnection()
		    .createStatement().executeQuery(sql);
	    if (openRS.next()) {
		paid = openRS.getBigDecimal(1);
	    }
	    if (paid == null) {
		paid = new BigDecimal(0.0);
	    }

	    vector_OrdersByPersonAndDay.clear();
	    final List<OrderByPersonAndDay> opdInstances = OrderByPersonAndDay
		    .getInstances();
	    for (OrderByPersonAndDay opd : opdInstances) {
		final DBProperty<java.sql.Date> property = opd
			.getProperty(OrderByPersonAndDay.DELIVERY_DATE);
		if (property.getValue().compareTo(weekSelection.first()) < 0
			|| property.getValue().compareTo(weekSelection.last()) > 0)
		    continue;

		vector_OrdersByPersonAndDay
			.add(new Vector(opd.getProperties()));
	    }

	    return null;
	}

	protected void done() {
	    try {
		get();
		allOrdersForWeekModel.fireTableDataChanged();
		// table_AllOrdersForWeek.clearChangedCells();
		label_AllOrdersTotal.setText(MessageFormat.format(
			"{0,number,#,##0.00} €", total));
		label_AllOrdersPaid.setText(MessageFormat.format(
			"{0,number,#,##0.00} €", paid));
		label_AllOrdersRemaining.setText(MessageFormat.format(
			"{0,number,#,##0.00} €", total.subtract(paid)));
		checkBox_PlacedOrder
			.removeActionListener(checkBox_PlacedOrder_ActionListener);
		checkBox_PlacedOrder.setEnabled(placed != null);
		checkBox_PlacedOrder.setSelected(placed != null ? placed
			: false);
		checkBox_PlacedOrder
			.addActionListener(checkBox_PlacedOrder_ActionListener);
		model_OrdersByPersonAndDay.fireTableDataChanged();
		// table_OrdersByPersonAndDay.clearChangedCells();
	    } catch (Exception ex) {
		ex.printStackTrace();
		if (ex instanceof java.lang.InterruptedException)
		    return;
	    } finally {
		setStateIdle();
	    }
	}
    }

    class PopupMenuFactory {

	public JPopupMenu createMenu(JPopupMenu menu, Component source,
		Object target) {
	    final JPopupMenu popupMenu = new JPopupMenu();
	    popupMenu.add(new CreateOrModifyOrderAction(false));
	    popupMenu.add(new JSeparator());
	    popupMenu.add(new DeleteOrdersAction());
	    return popupMenu;
	}

	public void attachListeners(final Component c) {
	    c.addMouseListener(new MouseAdapter() {

		public void mousePressed(MouseEvent arg0) {
		    if (arg0.getButton() != MouseEvent.BUTTON3)
			return;
		    createMenu(null, c, null).show(c, arg0.getX(), arg0.getY());
		}
	    });
	}

    }

    abstract class OrdersAction extends AbstractAction {

	protected OrdersAction(String label) {
	    super(label);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @return {@inheritDoc}
	 * 
	 * @see javax.swing.AbstractAction#isEnabled()
	 */
	@Override
	public boolean isEnabled() {
	    return super.isEnabled()
		    && table_MyOrdersForWeek.getSelectedRows().length > 0;
	}

    }

    class DeleteOrdersAction extends OrdersAction {

	DeleteOrdersAction() {
	    super(
		    table_MyOrdersForWeek.getSelectedRows().length > 1 ? "Bestellungen löschen"
			    : "Bestellung löschen");
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param e
	 *            {@inheritDoc}
	 * 
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {
	    new SwingWorker<Object, Object>() {

		protected Object doInBackground() throws Exception {
		    setStateBusy();

		    int answer = JOptionPane
			    .showConfirmDialog(
				    MenuOrders.this,
				    "Sollen die Bestellungen wirklich gelöscht werden?",
				    "Bestellungen löschen",
				    JOptionPane.OK_CANCEL_OPTION,
				    JOptionPane.WARNING_MESSAGE, null);
		    if (answer != JOptionPane.OK_OPTION)
			return null;

		    for (Order o : getSelectedOrders()) {
			MenuOrders.getDBManager().delete(o);
		    }

		    return null;
		}

		protected void done() {
		    try {
			Object result = get();

			updateData();
		    } catch (Exception ex) {
			ex.printStackTrace();
			if (ex instanceof java.lang.InterruptedException)
			    return;
		    } finally {
			setStateIdle();
		    }
		}
	    }.execute();
	}

    }

    class SetPaidAction extends OrdersAction {

	private final Boolean paid;

	public SetPaidAction(Boolean paid) {
	    super("Auf " + (paid ? "`Bezahlt`" : "`Nicht bezahlt`") + " setzen");
	    this.paid = paid;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param e
	 *            {@inheritDoc}
	 * 
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {
	    new SwingWorker<Object, Object>() {

		protected Object doInBackground() throws Exception {
		    setStateBusy();
		    List<Order> selectedOrders = getSelectedOrders();
		    for (Order o : selectedOrders) {
			o.getProperty(Order.PAID).setValue(paid);
			MenuOrders.getDBManager().save(o);
		    }

		    return null;
		}

		protected void done() {
		    try {
			Object result = get();
			updateData();
		    } catch (Exception ex) {
			ex.printStackTrace();
			if (ex instanceof java.lang.InterruptedException)
			    return;
		    } finally {
			setStateIdle();
		    }
		}
	    }.execute();
	}

    }

    class CreateOrModifyOrderAction extends OrdersAction {

	private final boolean create;

	public CreateOrModifyOrderAction(boolean create) {
	    super(create ? "Neue Bestellung" : "Bestellung bearbeiten");
	    this.create = create;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param e
	 *            {@inheritDoc}
	 * 
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {
	    new SwingWorker<List<Article>, Object>() {

		List<Article> articles = null;
		Order order = null;

		protected List<Article> doInBackground() throws Exception {
		    setStateBusy();
		    if (!create) {
			order = getSelectedOrders().get(0);
		    } else {
			order = new Order();
		    }

		    articles = Article.getInstances();
		    return articles;
		}

		protected void done() {
		    try {
			List<Article> result = get();

			final JDialog dialog = new OrderDialog(MenuOrders.this,
				create, order, persons, result,
				monthView.getSelection(),
				(Person) list_Persons.getSelectedValue(),
				!create);
			dialog.setVisible(true);

			updateData();
		    } catch (Exception ex) {
			if (ex instanceof java.lang.InterruptedException)
			    return;
			showException(ex);
		    } finally {
			setStateIdle();
		    }
		}
	    }.execute();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @return {@inheritDoc}
	 * 
	 * @see de.dhiller.menuorders.MenuOrders.OrdersAction#isEnabled()
	 */
	@Override
	public boolean isEnabled() {
	    return (list_Persons != null && list_Persons.getSelectedValue() != null)
		    && (this.create ? true : table_MyOrdersForWeek
			    .getSelectedRows().length == 1);
	}

    }

    class BigDecimalRenderer extends DefaultTableCellRenderer {

	/**
	 * Erzeugt eine neue <code>MenuOrders.BigDecimalRenderer</code>-Instanz.
	 * 
	 */
	public BigDecimalRenderer() {
	    super();
	    setHorizontalAlignment(JLabel.TRAILING);
	}

	@Override
	public Component getTableCellRendererComponent(JTable table,
		Object value, boolean isSelected, boolean hasFocus, int row,
		int column) {
	    final Component result = super.getTableCellRendererComponent(table,
		    value, isSelected, hasFocus, row, column);
	    if (result instanceof JLabel && value instanceof BigDecimal) {
		BigDecimal bd = (BigDecimal) value;
		JLabel l = (JLabel) result;
		l.setText(MessageFormat.format("{0,number,#,##0.00} €", bd));
	    }
	    return result;
	}
    }

    // TODO: Port to new highlighter api
    // private final class TodayHighlighter extends Highlighter {
    //
    // private final JXTable t;
    //
    // TodayHighlighter( JXTable t ) {
    // this.t = t;
    // }
    //
    // @Override
    // protected Color computeBackground( Component renderer , ComponentAdapter
    // adapter ) {
    // Color background = super.computeBackground( renderer , adapter );
    // if ( ! adapter.isSelected() ) {
    // for ( int i = 0 , n = adapter.getColumnCount() ; i < n ; i++ ) {
    // final Object value = adapter.getValueAt( adapter.row , i );
    // if ( value instanceof Date ) {
    // Calendar c = Calendar.getInstance();
    // c.setTime( ( Date ) value );
    // Calendar now = Calendar.getInstance();
    // if ( now.get( Calendar.YEAR ) == c.get( Calendar.YEAR )
    // && now.get( Calendar.DAY_OF_YEAR ) == c.get( Calendar.DAY_OF_YEAR ) ) {
    // background = merge( TODAY_COLOR , ( adapter.row % 2 > 0 ? 10 : 2 ) ,
    // adapter
    // .getComponent().getBackground() , 1 );
    // }
    // }
    // }
    // }
    // return background;
    // }
    // }

    public static Color merge(Color first, int weightOfFirst, Color second,
	    int weightOfSecond) {
	if (weightOfFirst <= 0)
	    return second;
	else if (weightOfSecond <= 0)
	    return first;
	return new Color(weightedAverage(first.getRed(), weightOfFirst,
		second.getRed(), weightOfSecond), weightedAverage(
		first.getGreen(), weightOfFirst, second.getGreen(),
		weightOfSecond), weightedAverage(first.getBlue(),
		weightOfFirst, second.getBlue(), weightOfSecond));
    }

    public static int weightedAverage(int first, int firstWeight, int second,
	    int secondWeight) {
	return (int) (((first * firstWeight) + second * secondWeight) / (double) (firstWeight + secondWeight));
    }

    // TODO: Port to new highlighter api
    // private final class AlternateDayHighlighter extends Highlighter {
    //
    // private final JXTable t;
    //
    // AlternateDayHighlighter( JXTable t ) {
    // this.t = t;
    // }
    //
    // @Override
    // protected Color computeBackground( Component renderer , ComponentAdapter
    // adapter ) {
    // Color background = super.computeBackground( renderer , adapter );
    // if ( ! adapter.isSelected() ) {
    // for ( int i = 0 , n = adapter.getColumnCount() ; i < n ; i++ ) {
    // final Object value = adapter.getValueAt( adapter.row , i );
    // if ( value instanceof Date ) {
    // Calendar c = Calendar.getInstance();
    // c.setTime( ( Date ) value );
    // boolean even = ( c.get( Calendar.DAY_OF_YEAR ) % 2 ) == 0;
    // background = ( even ? Color.LIGHT_GRAY : Color.WHITE );
    // break;
    // }
    // }
    // }
    // return background;
    // }
    // }

    // Constructors
    // ==================================================================================================================

    // Public
    // ----------------------------------------------------------------------------------------------

    /**
     * Erzeugt eine neue <code>MenuOrders</code>-Instanz.
     * 
     * @throws DBManagerException
     */
    public MenuOrders() throws DBManagerException {
	super("Menü Bestellungen");
	this.setName(FRAME);
	this.setLayout(new BorderLayout());

	setDBManager();
	setUpWindowListener();

	// DateRenderer dr = new DateRenderer();

	final JTabbedPane tabbedPane = new JTabbedPane();
	tabbedPane.setName(TABBED_PANE);
	tabbedPane.addTab("Deine Bestellungen der Woche",
		createOrdersForOnePersonAndWeek());
	tabbedPane.addTab("Alle Bestellungen der Woche",
		createAllOrdersForWeek());
	tabbedPane.addTab("Bestellungen der Woche je Person",
		createOrdersByPersonAndDay());
	tabbedPane.addTab("Bestellungen des Monats je Person",
		createOrdersByPersonPerMonth());

	this.add(new JSplitPane(
		JSplitPane.HORIZONTAL_SPLIT, tabbedPane, createSelectWeek()));

	this.pack();
	this.setLocationRelativeTo(null);
    }

    private JPanel createSelectWeek() {
	JPanel panel_SelectWeek = new JPanel();
	panel_SelectWeek.setLayout(new BoxLayout(panel_SelectWeek,
		BoxLayout.PAGE_AXIS));
	panel_SelectWeek.add(new JLabel("Woche auswählen"));
	panel_SelectWeek.add(Box.createRigidArea(new Dimension(0, 5)));
	createMonthView();
	panel_SelectWeek.add(monthView);
	panel_SelectWeek.add(Box.createRigidArea(new Dimension(0, 5)));
	panel_SelectWeek.add(new JLabel("Wer bist Du?"));

	createPersonList();
	panel_SelectWeek.add(new JScrollPane(list_Persons));

	populatePersonList();

	panel_SelectWeek.add(Box.createVerticalGlue());
	return panel_SelectWeek;
    }

    private JPanel createOrdersByPersonPerMonth() {
	final JPanel panelOrdersByPersonPerMonth = new JPanel();
	panelOrdersByPersonPerMonth.setName(ORDERS_BY_PERSON_PER_MONTH);
	panelOrdersByPersonPerMonth.setLayout(new BorderLayout());
	final JPanel monthSelector = new JPanel();
	final Calendar calendar = Calendar.getInstance();
	final JSpinner monthSpinner = new JSpinner(new SpinnerNumberModel(
		calendar.get(Calendar.MONTH), 1, 12, 1));
	monthSpinner.setName(MONTH_SPINNER);
	monthSpinner.setValue(calendar.get(Calendar.MONTH) + 1);
	monthSpinner.setMinimumSize(new Dimension(50, (int) monthSpinner
		.getMinimumSize().getHeight()));
	monthSpinner.setPreferredSize(new Dimension(50, (int) monthSpinner
		.getMinimumSize().getHeight()));
	monthSelector.add(monthSpinner);
	final JSpinner yearSpinner = new JSpinner(new SpinnerNumberModel(
		calendar.get(Calendar.YEAR), 2000, calendar.get(Calendar.YEAR),
		1));
	yearSpinner.setName(YEAR_SPINNER);
	yearSpinner.setEditor(new JSpinner.NumberEditor(yearSpinner, "#"));
	yearSpinner.setMinimumSize(new Dimension(100, (int) yearSpinner
		.getMinimumSize().getHeight()));
	yearSpinner.setPreferredSize(new Dimension(100, (int) yearSpinner
		.getMinimumSize().getHeight()));
	monthSelector.add(yearSpinner);
	panelOrdersByPersonPerMonth.add(monthSelector, BorderLayout.NORTH);
	final JXTable xTableOrdersByPersonPerMonth = new JXTable();
	xTableOrdersByPersonPerMonth.setEditable(false);
	panelOrdersByPersonPerMonth.add(new JScrollPane(
		xTableOrdersByPersonPerMonth), BorderLayout.CENTER);
	final ExportOrdersByPersonPerMonth exportOrdersByPersonPerMonth = new ExportOrdersByPersonPerMonth(
		"Exportieren...", panelOrdersByPersonPerMonth);
	panelOrdersByPersonPerMonth.add(new JButton(
		exportOrdersByPersonPerMonth), BorderLayout.SOUTH);

	new OrdersByPersonPerMonthUpdater(xTableOrdersByPersonPerMonth,
		yearSpinner, monthSpinner, exportOrdersByPersonPerMonth)
		.execute();
	monthSpinner.addChangeListener(new ChangeListener() {

	    @Override
	    public void stateChanged(ChangeEvent e) {
		new OrdersByPersonPerMonthUpdater(xTableOrdersByPersonPerMonth,
			yearSpinner, monthSpinner, exportOrdersByPersonPerMonth)
			.execute();
	    }
	});
	yearSpinner.addChangeListener(new ChangeListener() {

	    @Override
	    public void stateChanged(ChangeEvent e) {
		new OrdersByPersonPerMonthUpdater(xTableOrdersByPersonPerMonth,
			yearSpinner, monthSpinner, exportOrdersByPersonPerMonth)
			.execute();
	    }
	});
	return panelOrdersByPersonPerMonth;
    }

    private void populatePersonList() {
	new SwingWorker<Person[], Object>() {

	    protected Person[] doInBackground() throws Exception {
		persons = (Person[]) Person.getInstances().toArray(
			new Person[Person.getInstances().size()]);
		return persons;
	    }

	    protected void done() {
		try {
		    Person[] result = get();
		    list_Persons.setModel(new DefaultComboBoxModel(persons));
		    list_Persons.getSelectionModel().addListSelectionListener(
			    new ListSelectionListener() {

				public void valueChanged(ListSelectionEvent e) {
				    Person p = (Person) list_Persons
					    .getSelectedValue();
				    Preferences.userNodeForPackage(
					    MenuOrders.class).put("Person",
					    p != null ? p.toString() : "");
				    new MyOrdersGetter().execute();
				}
			    });
		    String lastPerson = Preferences.userNodeForPackage(
			    MenuOrders.class).get("Person", null);
		    if (lastPerson != null) {
			for (Person p : persons) {
			    if (p.getProperty(Person.NAME).getValue()
				    .equals(lastPerson)) {
				list_Persons.setSelectedValue(p, true);
				break;
			    }
			}
		    } else {
			list_Persons.setSelectedValue(null, false);
		    }

		    Date d = new Date();
		    monthView.setSelectionInterval(d, d);

		    new Thread() {

			public void run() {
			    updateData();
			}
		    }.start();
		} catch (Exception ex) {
		    if (ex instanceof java.lang.InterruptedException)
			return;
		    showException(ex);
		} finally {
		    setStateIdle();
		}
	    }
	}.execute();
    }

    private void setDBManager() {
	this.dbManager = MenuOrders.getDBManager();
    }

    private void setUpWindowListener() {
	this.addWindowListener(new WindowAdapter() {

	    /**
	     * {@inheritDoc}
	     * 
	     * @param e
	     *            {@inheritDoc}
	     * 
	     * @see java.awt.event.WindowAdapter#windowClosing(java.awt.event.WindowEvent)
	     */
	    @Override
	    public void windowClosing(WindowEvent e) {
		try {
		    setStateBusy();
		} catch (InterruptedException ignore) {
		}
		try {
		    dbManager.cleanUp();
		    System.exit(0);
		} catch (DBManagerException ex) {
		    showException(ex);
		    System.exit(1);
		}
	    }
	});
    }

    private void createPersonList() {
	list_Persons = new JList();
	list_Persons.setName(PERSON_LIST);
	list_Persons.setCellRenderer(new DefaultListCellRenderer() {

	    @Override
	    public Component getListCellRendererComponent(JList list,
		    Object value, int index, boolean isSelected,
		    boolean cellHasFocus) {
		final Component result = super.getListCellRendererComponent(
			list, value, index, isSelected, cellHasFocus);
		if (result instanceof JLabel && value instanceof Person) {
		    Person p = (Person) value;
		    BigDecimal ordered = (ordereds.containsKey(p) ? ordereds
			    .get(p) : new BigDecimal(0.0));
		    BigDecimal paid = (paids.containsKey(p) ? paids.get(p)
			    : new BigDecimal(0.0));
		    JLabel l = (JLabel) result;
		    l.setText(MessageFormat
			    .format("{0} (Bestellt: {1,number,#,##0.00} €, Bezahlt: {2,number,#,##0.00} €)",
				    p, ordered, paid));
		    l.setForeground(ordered.equals(paid) ? (isSelected ? list_Persons
			    .getSelectionForeground() : list_Persons
			    .getForeground())
			    : (isSelected ? Color.ORANGE : Color.RED));
		}
		return result;
	    }
	});
    }

    private JPanel createOrdersByPersonAndDay() {
	JPanel panel_OrdersByPersonAndDay = new JPanel();
	panel_OrdersByPersonAndDay.setName(ORDERS_BY_PERSON_AND_DAY);
	panel_OrdersByPersonAndDay.setLayout(new BorderLayout());
	vector_OrdersByPersonAndDay = new Vector();
	model_OrdersByPersonAndDay = new DBObjectTableModel(
		new OrderByPersonAndDay(), vector_OrdersByPersonAndDay);
	table_OrdersByPersonAndDay = new JXTable(model_OrdersByPersonAndDay);
	table_OrdersByPersonAndDay.setName(ORDERS_BY_PERSON_AND_DAY_TABLE);
	// TODO: Reactivate
	// table_OrdersByPersonAndDay.addHighlighter( new
	// AlternateDayHighlighter( table_OrdersByPersonAndDay ) );
	table_OrdersByPersonAndDay.setDefaultRenderer(Date.class,
		new DefaultTableCellRenderer() {

		    @Override
		    public Component getTableCellRendererComponent(
			    JTable table, Object value, boolean isSelected,
			    boolean hasFocus, int row, int column) {
			final JLabel lable = (JLabel) super
				.getTableCellRendererComponent(table, value,
					isSelected, hasFocus, row, column);
			if (value instanceof Date) {
			    Date d = (Date) value;
			    lable.setText(new SimpleDateFormat("EEEE, d.M.y")
				    .format(d));
			}
			return lable;
		    }
		});
	((TableColumnExt) table_OrdersByPersonAndDay.getColumns(true).get(0))
		.setVisible(false);
	panel_OrdersByPersonAndDay.add(new JScrollPane(
		table_OrdersByPersonAndDay));
	final JButton button_Print = new JButton("Drucken");
	button_Print.setName(PRINT_BUTTON);
	button_Print.addActionListener(new ActionListener() {

	    public void actionPerformed(ActionEvent e) {
		try {
		    table_OrdersByPersonAndDay.print();
		} catch (PrinterException e1) {
		    showException(e1);
		}
	    }
	});
	panel_OrdersByPersonAndDay.add(button_Print, BorderLayout.SOUTH);
	return panel_OrdersByPersonAndDay;
    }

    private JPanel createOrdersForOnePersonAndWeek() {
	final JPanel panel_OrdersForOnePersonAndWeek = new JPanel(
		new BorderLayout());
	panel_OrdersForOnePersonAndWeek.setName(ORDERS_FOR_ONE_PERSON_AND_WEEK);
	vector_MyOrdersForWeek = new Vector();
	myOrdersForWeekModel = new DBObjectTableModel(
		new OrderCostPerPersonAndDay(), vector_MyOrdersForWeek);
	table_MyOrdersForWeek = new JXTable();
	table_MyOrdersForWeek.setName(ORDERS_FOR_ONE_PERSON_AND_WEEK_TABLE);
	table_MyOrdersForWeek.setDefaultRenderer(BigDecimal.class,
		bigDecimalRenderer);
	// table_MyOrdersForWeek.setDefaultRenderer( Date.class , dr );

	table_MyOrdersForWeek.setModel(myOrdersForWeekModel);

	table_MyOrdersForWeek.getColumnExt(8).setVisible(false);
	table_MyOrdersForWeek.getColumnExt(4).setVisible(false);
	table_MyOrdersForWeek.getColumnExt(0).setVisible(false);

	// TODO: Reactivate
	// table_MyOrdersForWeek.addHighlighter( new TodayHighlighter(
	// table_MyOrdersForWeek ) );

	final JScrollPane scrollPane_MyOrdersForWeek = new JScrollPane(
		table_MyOrdersForWeek);
	panel_OrdersForOnePersonAndWeek.add(scrollPane_MyOrdersForWeek,
		BorderLayout.CENTER);

	final JPanel panel_MarkPaidUnpaid = new JPanel();
	panel_MarkPaidUnpaid.setName(TOGGLE_PAID);
	panel_MarkPaidUnpaid.setLayout(new BoxLayout(panel_MarkPaidUnpaid,
		BoxLayout.LINE_AXIS));
	checkBox_MarkPaidUnpaid = new JCheckBox("Bezahlt");
	checkBox_MarkPaidUnpaid.setName(TOGGLE_PAID_UNPAID);
	setPaidUnpaidActionListener = new ActionListener() {

	    public void actionPerformed(ActionEvent e) {

		final boolean setPaid = checkBox_MarkPaidUnpaid.isSelected();

		new SwingWorker<Object, Object>() {

		    protected Object doInBackground() throws Exception {

			setStateBusy();

			for (Order o : getOrders()) {
			    DBProperty<Boolean> p = o.getProperty(Order.PAID);
			    p.setValue(setPaid);
			    dbManager.save(o);
			}

			return null;
		    }

		    protected void done() {
			try {
			    Object result = get();
			    updateData();
			} catch (Exception ex) {
			    ex.printStackTrace();
			    if (ex instanceof java.lang.InterruptedException)
				return;
			} finally {
			    setStateIdle();
			}
		    }
		}.execute();
	    }

	};
	checkBox_MarkPaidUnpaid.addActionListener(setPaidUnpaidActionListener);
	panel_MarkPaidUnpaid.add(checkBox_MarkPaidUnpaid);
	panel_MarkPaidUnpaid.add(Box.createHorizontalGlue());
	button_CreateNewOrder = new JButton(new CreateOrModifyOrderAction(true));
	button_CreateNewOrder.setName(CREATE_NEW_ORDER);
	panel_MarkPaidUnpaid.add(button_CreateNewOrder);
	panel_MarkPaidUnpaid.setBorder(BorderFactory.createEmptyBorder(5, 0, 0,
		0));
	panel_OrdersForOnePersonAndWeek.add(panel_MarkPaidUnpaid,
		BorderLayout.SOUTH);
	table_MyOrdersForWeek
		.addMouseListener(new SelectRowAndColumnOnRightClickMouseAdapter());
	final PopupMenuFactory popupMenuFactory = new PopupMenuFactory();
	popupMenuFactory.attachListeners(table_MyOrdersForWeek);
	popupMenuFactory.attachListeners(scrollPane_MyOrdersForWeek);
	panel_OrdersForOnePersonAndWeek.setBorder(BorderFactory
		.createEmptyBorder(5, 5, 5, 5));
	return panel_OrdersForOnePersonAndWeek;
    }

    private JPanel createAllOrdersForWeek() {

	vector_AllOrdersForWeek = new Vector();
	allOrdersForWeekModel = new DBObjectTableModel(
		new DailyTotalByMenuWithPrice(), vector_AllOrdersForWeek);

	table_AllOrdersForWeek = new JXTable();
	table_AllOrdersForWeek.setName(ALL_ORDERS_FOR_WEEK_TABLE);
	table_AllOrdersForWeek.setDefaultRenderer(BigDecimal.class,
		bigDecimalRenderer);
	// table_AllOrdersForWeek.setDefaultRenderer( Date.class , dr );
	table_AllOrdersForWeek.setModel(allOrdersForWeekModel);
	// TODO: Reactivate
	// table_AllOrdersForWeek.addHighlighter( new TodayHighlighter(
	// table_AllOrdersForWeek ) );

	final JPanel panel_AllOrdersForWeek = new JPanel();
	panel_AllOrdersForWeek.setName(ALL_ORDERS_FOR_WEEK);
	panel_AllOrdersForWeek.setLayout(new BoxLayout(panel_AllOrdersForWeek,
		BoxLayout.PAGE_AXIS));
	final Box tableBox = Box.createHorizontalBox();
	tableBox.add(new JScrollPane(table_AllOrdersForWeek));
	panel_AllOrdersForWeek.add(tableBox);

	panel_AllOrdersForWeek.add(Box.createRigidArea(new Dimension(0, 5)));

	label_AllOrdersTotal = new JLabel("0.0 €");
	label_AllOrdersTotal.setName(ALL_ORDERS_TOTAL);
	label_AllOrdersTotal.setHorizontalAlignment(JLabel.RIGHT);
	final Dimension preferredSize = new Dimension(50,
		((int) label_AllOrdersTotal.getPreferredSize().getHeight()));
	label_AllOrdersTotal.setPreferredSize(preferredSize);
	final Box label_AllOrdersTotal_Box = Box.createHorizontalBox();
	label_AllOrdersTotal_Box.add(Box.createHorizontalGlue());
	label_AllOrdersTotal_Box.add(new JLabel("Gesamt"));
	label_AllOrdersTotal_Box.add(label_AllOrdersTotal);
	panel_AllOrdersForWeek.add(label_AllOrdersTotal_Box);

	panel_AllOrdersForWeek.add(Box.createRigidArea(new Dimension(0, 5)));

	label_AllOrdersPaid = new JLabel("0.0 €");
	label_AllOrdersPaid.setName(ALL_ORDERS_PAID);
	label_AllOrdersPaid.setPreferredSize(preferredSize);
	label_AllOrdersPaid.setHorizontalAlignment(JLabel.RIGHT);
	final Box label_AllOrdersPaid_Box = Box.createHorizontalBox();
	label_AllOrdersPaid_Box.add(Box.createHorizontalGlue());
	label_AllOrdersPaid_Box.add(new JLabel("Bezahlt"));
	label_AllOrdersPaid_Box.add(label_AllOrdersPaid);
	panel_AllOrdersForWeek.add(label_AllOrdersPaid_Box);

	panel_AllOrdersForWeek.add(Box.createRigidArea(new Dimension(0, 5)));

	label_AllOrdersRemaining = new JLabel("0.0 €");
	label_AllOrdersRemaining.setName(ALL_ORDERS_REMAINING);
	label_AllOrdersRemaining.setPreferredSize(preferredSize);
	label_AllOrdersRemaining.setHorizontalAlignment(JLabel.RIGHT);
	final Box label_AllOrdersRemaining_Box = Box.createHorizontalBox();
	checkBox_PlacedOrder = new JCheckBox("Bestellt");
	checkBox_PlacedOrder.setName(TOGGLE_ORDER_PLACED);
	checkBox_PlacedOrder_ActionListener = new ActionListener() {

	    public void actionPerformed(ActionEvent e) {
		final boolean placed = checkBox_PlacedOrder.isSelected();
		final SortedSet<Date> weekSelection = getWeekSelection();

		new SwingWorker<Object, Object>() {

		    protected Object doInBackground() throws Exception {
			setStateBusy();
			final String sql = "UPDATE Orders " + "SET Placed = "
				+ placed + " WHERE DeliveryDate BETWEEN '"
				+ DATEFORMAT.format(weekSelection.first())
				+ "' AND '"
				+ DATEFORMAT.format(weekSelection.last())
				+ "';";
			final int updateCount = MenuOrders.getDBManager()
				.getConnection().createStatement()
				.executeUpdate(sql);

			return null;
		    }

		    protected void done() {
			try {
			    get();
			    // table_AllOrdersForWeek.clearChangedCells();
			    // table_MyOrdersForWeek.clearChangedCells();
			    updateData();
			} catch (Exception ex) {
			    if (ex instanceof java.lang.InterruptedException)
				return;
			    showException(ex);
			} finally {
			    setStateIdle();
			}
		    }
		}.execute();
	    }

	};
	checkBox_PlacedOrder
		.addActionListener(checkBox_PlacedOrder_ActionListener);
	label_AllOrdersRemaining_Box.add(checkBox_PlacedOrder);
	label_AllOrdersRemaining_Box.add(Box.createHorizontalGlue());
	label_AllOrdersRemaining_Box.add(new JLabel("Rest"));
	label_AllOrdersRemaining_Box.add(label_AllOrdersRemaining);
	panel_AllOrdersForWeek.add(label_AllOrdersRemaining_Box);

	panel_AllOrdersForWeek.setBorder(BorderFactory.createEmptyBorder(5, 5,
		5, 5));
	return panel_AllOrdersForWeek;
    }

    private void createMonthView() {
	monthView = new JXMonthView();
	monthView.setName(MONTH_VIEW);
	monthView.setPreferredColumnCount(1);
	monthView.setPreferredRowCount(1);
	monthView.setPreferredSize(new Dimension(150, 150));
	monthView.setSelectionMode(SelectionMode.SINGLE_SELECTION);
	monthView.setFirstDayOfWeek(Calendar.MONDAY);
	monthView.setTraversable(true);
	monthView.addActionListener(new ActionListener() {

	    public void actionPerformed(ActionEvent e) {
		updateData();
	    }
	});
	monthView.setDayForeground(Calendar.SATURDAY, Color.GRAY);
	monthView.setDayForeground(Calendar.SUNDAY, Color.GRAY);
	monthView.setBorder(BorderFactory
		.createBevelBorder(BevelBorder.LOWERED));
    }

    // Class methods
    // ==================================================================================================================

    // Public
    // ----------------------------------------------------------------------------------------------

    /**
     * @param args
     *            <ul>
     *            <li>args[0] = hostname</li>
     *            <li>args[1] = database name</li>
     *            </ul>
     * @throws UnsupportedLookAndFeelException
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws ClassNotFoundException
     */
    public static void main(String[] args) throws ClassNotFoundException,
	    InstantiationException, IllegalAccessException,
	    UnsupportedLookAndFeelException {
	UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

	try {
	    if (args.length > 0) {
		String hostname = args[0];
		String databaseName = args[1];
		setUpDBManagerWithRemoteConnection(hostname, databaseName);
	    } else {
		setUpDBManager(DBManager.newFileConnectionProperties(
			MenuOrders.FILE_NAME, "sa", ""));
	    }
	} catch (DBManagerException e1) {
	    e1.printStackTrace();
	}

	SwingUtilities.invokeLater(new Runnable() {

	    public void run() {
		MenuOrders menuOrders = null;
		try {
		    menuOrders = new MenuOrders();
		    menuOrders.setVisible(true);
		} catch (DBManagerException e) {
		    e.printStackTrace();
		    showException(new RuntimeException(
			    "Es konnte nicht auf die Datenbankdatei zugegriffen werden.\n"
				    + "Wahrscheinlich wird sie gerade verwendet!",
			    e));
		    System.exit(1);
		}
	    }
	});
    }

    public static DBManager getDBManager() {
	return dbManagerInstance;
    }

    public static void setDbManager(DBManager dbManagerInstance) {
	MenuOrders.dbManagerInstance = Test.notNull(dbManagerInstance);
    }

    protected void checkForChanges() {
    }

    private static void setUpDBManagerWithRemoteConnection(String hostname,
	    String databaseName) throws DBManagerException {
	ConnectionProperties connectionProperties = DBManager
		.newRemoteConnectionProperties(hostname, databaseName);
	setUpDBManager(connectionProperties);
    }

    private static void setUpDBManager(ConnectionProperties connectionProperties)
	    throws DBManagerException {
	DBManager.setProperties(connectionProperties);
	setDbManager(new DBManager(connectionProperties));
    }

    private List<Order> getSelectedOrders() {
	List<Order> selectedOrders = new ArrayList<Order>();
	Map<Integer, Order> ordersMap = new Hashtable<Integer, Order>();
	try {
	    for (Order o : Order.getInstances()) {
		ordersMap.put((Integer) o.getIdentityProperty().getValue(), o);
	    }
	    for (int selectedRow : table_MyOrdersForWeek.getSelectedRows()) {
		for (Object p : (Vector) vector_MyOrdersForWeek
			.get(selectedRow)) {
		    DBProperty prop = (DBProperty) p;
		    if (prop.getField() == OrderCostPerPersonAndDay.ORDER_ID) {
			selectedOrders.add(ordersMap.get(prop.getValue()));
			break;
		    }
		}
	    }
	} catch (DBManagerException e) {
	    showException(e);
	}
	return selectedOrders;
    }

    private List<Order> getOrders() {
	List<Order> selectedOrders = new ArrayList<Order>();
	Map<Integer, Order> ordersMap = new Hashtable<Integer, Order>();
	try {
	    for (Order o : Order.getInstances()) {
		ordersMap.put((Integer) o.getIdentityProperty().getValue(), o);
	    }
	    for (int i = 0, n = table_MyOrdersForWeek.getRowCount(); i < n; i++) {
		for (Object p : (Vector) vector_MyOrdersForWeek.get(i)) {
		    DBProperty prop = (DBProperty) p;
		    if (prop.getField() == OrderCostPerPersonAndDay.ORDER_ID) {
			selectedOrders.add(ordersMap.get(prop.getValue()));
			break;
		    }
		}
	    }
	} catch (DBManagerException e) {
	    showException(e);
	}
	return selectedOrders;
    }

    private SortedSet<Date> getWeekSelection() {
	final SortedSet<Date> selection = monthView.getSelection();
	return getWeekSelection(selection);
    }

    private SortedSet<Date> getWeekSelection(final SortedSet<Date> selection) {
	final SortedSet<Date> result = new TreeSet<Date>();
	final Calendar fromCal = Calendar.getInstance();
	fromCal.setTime(selection.size() > 0 ? selection.first() : new Date());
	fromCal.set(Calendar.DAY_OF_WEEK, fromCal.getFirstDayOfWeek());
	result.add(fromCal.getTime());
	final Calendar toCal = Calendar.getInstance();
	toCal.setTime(selection.size() > 0 ? selection.last() : new Date());
	toCal.set(Calendar.DAY_OF_WEEK, fromCal.getFirstDayOfWeek() + 6);
	result.add(toCal.getTime());
	return result;
    }

    private void updateData() {
	new AllOrdersGetter().execute();
	new MyOrdersGetter().execute();
	new PersonTotalsPerWeekGetter().execute();
    }

    public static void showException(Exception e) {
	e.printStackTrace();
	JOptionPane.showMessageDialog(null, e.getMessage(), e.getClass()
		.getName(), JOptionPane.ERROR_MESSAGE);
    }

    private void setStateIdle() {
	synchronized (state) {
	    state = IDLE;
	}
    }

    private void setStateBusy() throws InterruptedException {
	synchronized (state) {
	    while (state != IDLE) {
		state.wait(50);
	    }
	    state = BUSY;
	}
    }

}
