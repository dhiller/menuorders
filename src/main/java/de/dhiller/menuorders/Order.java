/*
 * Copyright (c) 2006-2011, dhiller, http://www.dhiller.de Daniel Hiller, Warendorfer Str. 47, 48231 Warendorf, NRW,
 * Germany, All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 * 
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and the following
 * disclaimer. - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution. - Neither the
 * name of dhiller nor the names of its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.dhiller.menuorders;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.dhiller.database.DBField;
import de.dhiller.database.DBManager.DBManagerException;
import de.dhiller.database.DBObject;
import de.dhiller.database.DBProperty;

/**
 * @author dhiller (creator)
 * @since 10.09.2006 15:55:13
 */
public class Order extends DBObject {

    // Declarations
    // ==================================================================================================================

    // Class ( public , package-private , protected , private - final first )
    // ----------------------------------------------------------------------------------------------
    public static final DBField<Integer> ID = new DBField<Integer>("Id", null,
	    Integer.class, true, 0);
    public static final DBField<Integer> PERSON_ID = new DBField<Integer>(
	    "PersonId", null, Integer.class, false, 1);
    public static final DBField<Integer> ARTICLE_ID = new DBField<Integer>(
	    "ArticleId", null, Integer.class, false, 2);
    public static final DBField<Integer> AMOUNT = new DBField<Integer>(
	    "Amount", null, Integer.class, false, 3);
    public static final DBField<Date> DELIVERY_DATE = new DBField<Date>(
	    "DeliveryDate", null, Date.class, false, 4);
    public static final DBField<Boolean> PAID = new DBField<Boolean>("Paid",
	    null, Boolean.class, false, 5);
    public static final DBField<Boolean> PLACED = new DBField<Boolean>(
	    "Placed", null, Boolean.class, false, 6);

    private static final List<DBField<?>> FIELDS;

    // Instance
    // ----------------------------------------------------------------------------------------------

    // Inner classes
    // ==================================================================================================================

    // Public
    // ----------------------------------------------------------------------------------------------

    // Constructors
    // ==================================================================================================================

    // Static
    // ----------------------------------------------------------------------------------------------

    static {
	List<DBField<?>> l = new ArrayList<DBField<?>>();
	l.add(ID);
	l.add(PERSON_ID);
	l.add(ARTICLE_ID);
	l.add(AMOUNT);
	l.add(DELIVERY_DATE);
	l.add(PAID);
	l.add(PLACED);
	FIELDS = Collections.<DBField<?>> unmodifiableList(l);
    }

    // Public
    // ----------------------------------------------------------------------------------------------

    /**
     * Erzeugt eine neue <code>Order</code>-Instanz.
     * 
     * @param tableName
     */
    public Order() {
	super("Orders");
	Set<DBProperty<?>> props = new HashSet<DBProperty<?>>();
	props.add(new DBProperty<Integer>(ID, null));
	props.add(new DBProperty<Integer>(PERSON_ID, null));
	props.add(new DBProperty<Integer>(ARTICLE_ID, null));
	props.add(new DBProperty<Integer>(AMOUNT, null));
	props.add(new DBProperty<Date>(DELIVERY_DATE, null));
	props.add(new DBProperty<Boolean>(PAID, Boolean.FALSE));
	props.add(new DBProperty<Boolean>(PLACED, Boolean.FALSE));
	setProperties(props);
    }

    // Protected
    // ----------------------------------------------------------------------------------------------

    /**
     * Erzeugt eine neue <code>Person</code>-Instanz.
     * 
     * @param propertiesForObject
     */
    protected Order(Set<DBProperty<?>> propertiesForObject) {
	super("Orders");
	setProperties(propertiesForObject);
    }

    // Class methods
    // ==================================================================================================================

    // Public
    // ----------------------------------------------------------------------------------------------

    public static List<Order> getInstances() throws DBManagerException {
	final ArrayList<Order> result = new ArrayList<Order>();
	Set<DBProperty<?>> filter = new HashSet<DBProperty<?>>();
	List<Set<DBProperty<?>>> properties = MenuOrders.getDBManager()
		.getPropertySets(new Order(), filter);
	for (Set<DBProperty<?>> propertiesForObject : properties) {
	    result.add(new Order(propertiesForObject));
	}
	return result;
    }

    // Instance methods
    // ==================================================================================================================

    // Public
    // ----------------------------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     * 
     * @return {@inheritDoc}
     * 
     * @see de.dhiller.database.DBObject#getFields()
     */
    @Override
    public List<DBField<?>> getFields() {
	return FIELDS;
    }

}
