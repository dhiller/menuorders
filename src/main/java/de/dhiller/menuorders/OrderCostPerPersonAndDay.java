/*
 * Copyright (c) 2006-2011, dhiller, http://www.dhiller.de
 * Daniel Hiller, Warendorfer Str. 47, 48231 Warendorf, NRW, Germany, 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 * 
 * - Redistributions of source code must retain the above copyright notice, this 
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, 
 *   this list of conditions and the following disclaimer in the documentation 
 *   and/or other materials provided with the distribution.
 * - Neither the name of dhiller nor the names of its
 *   contributors may  
 *   be used to endorse or promote products derived from this software without 
 *   specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.dhiller.menuorders;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.dhiller.database.DBField;
import de.dhiller.database.DBManager;
import de.dhiller.database.DBObject;
import de.dhiller.database.DBProperty;
import de.dhiller.database.DBManager.DBManagerException;


/**
 * @author dhiller (creator)
 * @since 10.09.2006 23:55:55
 */
public class OrderCostPerPersonAndDay extends DBObject {

  // Declarations
  // ==================================================================================================================

  // Class ( public , package-private , protected , private - final first )
  // ----------------------------------------------------------------------------------------------
  public static final DBField< Integer >            PERSON_ID     = new DBField< Integer >(
                                                                    "PersonID" , "ID Person" , Integer.class , false ,
                                                                    0 );
  public static final DBField< String >             PERSON_NAME   = new DBField< String >(
                                                                    "PersonName" , "Name" , String.class , false , 1 );
  public static final DBField< Date >            DELIVERY_DATE = new DBField< Date >(
                                                                    "DeliveryDate" , "Liefertermin" , Date.class ,
                                                                    false , 2 );
  public static final DBField< String >             ARTICLE_NAME  = new DBField< String >(
	    "ArticleName", "Menü", String.class, false, 3);
  public static final DBField< Integer >            ORDER_ID      = new DBField< Integer >(
                                                                    "OrderID" , "Auftrag ID" , Integer.class , false ,
                                                                    4 );
  public static final DBField< Integer >            AMOUNT        = new DBField< Integer >(
                                                                    "Amount" , "Anzahl bestellt" , Integer.class ,
                                                                    false , 5 );
  public static final DBField< BigDecimal >      PRICE         = new DBField< BigDecimal >(
                                                                    "Price" , "Einzelpreis" , BigDecimal.class , false ,
                                                                    6 );
  public static final DBField< BigDecimal >      TOTAL_PRICE   = new DBField< BigDecimal >(
                                                                    "TotalPrice" , "Gesamtpreis" , BigDecimal.class ,
                                                                    false , 7 );
  public static final DBField< Boolean >    PAID          = new DBField< Boolean >(
                                                                    "Paid" , "Bezahlt" , Boolean.class , false , 8 );
  public static final DBField< Boolean >    PLACED        = new DBField< Boolean >(
                                                                    "Placed" , "Bestellt" , Boolean.class , false , 9 );

  private static final List< DBField >              FIELDS;

  // Instance
  // ----------------------------------------------------------------------------------------------


  // Inner classes
  // ==================================================================================================================

  // Public
  // ----------------------------------------------------------------------------------------------


  // Constructors
  // ==================================================================================================================
  
  // Static
  // ----------------------------------------------------------------------------------------------
  
  static {
    List<DBField> l = new ArrayList< DBField >();
    l.add( PERSON_ID );
    l.add( PERSON_NAME );
    l.add( DELIVERY_DATE );
    l.add( ARTICLE_NAME );
    l.add( ORDER_ID );
    l.add( AMOUNT );
    l.add( PRICE );
    l.add( TOTAL_PRICE );
    l.add( PAID );
    l.add( PLACED );
    FIELDS = Collections.unmodifiableList( l );
  }
  

  // Public
  // ----------------------------------------------------------------------------------------------

  /**
   * Erzeugt eine neue <code>OrderCostPerPersonAndDay</code>-Instanz.
   *
   * @param tableName
   */
  public OrderCostPerPersonAndDay() {
    super( "OrderCostPerPersonAndDay" );
    Set< DBProperty< ? >> props = new HashSet< DBProperty< ? >>();
    props.add( new DBProperty< Integer >( PERSON_ID , null ) );
    props.add( new DBProperty< String >( PERSON_NAME , null ) );
    props.add( new DBProperty< Date >( DELIVERY_DATE , null ) );
    props.add( new DBProperty< String >( ARTICLE_NAME , null ) );
    props.add( new DBProperty< Integer >( ORDER_ID , null ) );
    props.add( new DBProperty< Integer >( AMOUNT , null ) );
    props.add( new DBProperty< BigDecimal >( PRICE , null ) );
    props.add( new DBProperty< BigDecimal >( TOTAL_PRICE , null ) );
    props.add( new DBProperty< Boolean >( PAID , null ) );
    setProperties( props );
  }
  
  /**
   * Erzeugt eine neue <code>Person</code>-Instanz.
   *
   * @param propertiesForObject
   */
  protected OrderCostPerPersonAndDay( Set< DBProperty< ? >> propertiesForObject ) {
    super("OrderCostPerPersonAndDay" );
    setProperties( propertiesForObject );
  }
  
  
  // Class methods
  // ==================================================================================================================
  
  // Public
  // ----------------------------------------------------------------------------------------------
  
  public static List<OrderCostPerPersonAndDay> getInstances() throws DBManagerException {
    final ArrayList< OrderCostPerPersonAndDay > result = new ArrayList<OrderCostPerPersonAndDay>();
    Set<DBProperty<?>> filter = new HashSet<DBProperty<?>>();
    List<Set<DBProperty<?>>> properties = MenuOrders.getDBManager().getPropertySets( new OrderCostPerPersonAndDay() , filter );
    for ( Set<DBProperty<?>> propertiesForObject : properties ) {
      result.add( new OrderCostPerPersonAndDay( propertiesForObject ) );
    }
    return result;
  }


  // Instance methods
  // ==================================================================================================================

  // Public
  // ----------------------------------------------------------------------------------------------

  /**
   * {@inheritDoc}
   *
   * @return {@inheritDoc}
   *
   * @see de.dhiller.database.DBObject#getFields()
   */
  @Override
  public List getFields() {
    return FIELDS;
  }

}
