/*
 * Copyright (c) 2006-2011, dhiller, http://www.dhiller.de
 * Daniel Hiller, Warendorfer Str. 47, 48231 Warendorf, NRW, Germany, 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 * 
 * - Redistributions of source code must retain the above copyright notice, this 
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, 
 *   this list of conditions and the following disclaimer in the documentation 
 *   and/or other materials provided with the distribution.
 * - Neither the name of dhiller nor the names of its
 *   contributors may  
 *   be used to endorse or promote products derived from this software without 
 *   specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.dhiller.menuorders;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.SortedSet;

import javax.swing.*;

import org.jdesktop.swingx.JXDatePicker;
import javax.swing.SwingWorker;

import de.dhiller.database.DBManager.DBManagerException;
import de.dhiller.database.DBObject;
import de.dhiller.util.Test;

/**
 * @author dhiller (creator)
 * @since 12.09.2006 09:40:23
 */
public class OrderDialog extends JDialog {

    private final class CancelAction extends AbstractAction {
	private CancelAction(String name) {
	    super(name);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
	    OrderDialog.this.dispose();
	}
    }

    /*
     * TODO: Refactoring von {@link OrderSaverAndDialogIncrementor} und {@link
     * OrderSaver}
     */
    private abstract class BaseOrderSaver extends SwingWorker<Object, Object> {

	private final JFormattedTextField amountTextField;
	private final JComboBox articleComboBox;
	private final JXDatePicker datePicker;
	private final JComboBox personComboBox;

	private BaseOrderSaver(
		JFormattedTextField amountTextField, JComboBox articleComboBox,
		JXDatePicker datePicker, JComboBox personComboBox) {
	    this.amountTextField = Test.notNull(amountTextField);
	    this.articleComboBox = Test.notNull(articleComboBox);
	    this.datePicker = Test.notNull(datePicker);
	    this.personComboBox = Test.notNull(personComboBox);
	}

    }

    private final class OrderSaverAndDialogIncrementor extends
	    SwingWorker<Object, Object> {

	private final JFormattedTextField amountTextField;
	private final JComboBox articleComboBox;
	private final JXDatePicker datePicker;
	private final JComboBox personComboBox;

	private OrderSaverAndDialogIncrementor(
		JFormattedTextField amountTextField, JComboBox articleComboBox,
		JXDatePicker datePicker, JComboBox personComboBox) {
	    this.amountTextField = amountTextField;
	    this.articleComboBox = articleComboBox;
	    this.datePicker = datePicker;
	    this.personComboBox = personComboBox;
	}

	protected Object doInBackground() throws Exception {
	    saveOrder(datePicker, personComboBox, articleComboBox,
		    amountTextField);
	    OrderDialog.this.order = new Order();
	    return null;
	}

	protected void done() {
	    try {
		get();
		final Calendar instance = Calendar.getInstance();
		instance.setTime(datePicker.getDate());
		instance.add(Calendar.DAY_OF_MONTH, 1);
		datePicker.setDate(instance.getTime());
	    } catch (Exception ex) {
		if (ex instanceof java.lang.InterruptedException)
		    return;
		MenuOrders.showException(ex);
		return;
	    }
	}
    }

    private final class OrderSaver extends SwingWorker<Object, Object> {

	private final JXDatePicker datePicker;
	private final JComboBox articleComboBox;
	private final JComboBox personComboBox;
	private final JFormattedTextField amountTextField;

	private OrderSaver(JXDatePicker datePicker, JComboBox articleComboBox,
		JComboBox personComboBox, JFormattedTextField amountTextField) {
	    this.datePicker = datePicker;
	    this.articleComboBox = articleComboBox;
	    this.personComboBox = personComboBox;
	    this.amountTextField = amountTextField;
	}

	protected Object doInBackground() throws Exception {
	    saveOrder(datePicker, personComboBox, articleComboBox,
		    amountTextField);
	    return null;
	}

	protected void done() {
	    try {
		get();
	    } catch (Exception ex) {
		if (ex instanceof java.lang.InterruptedException)
		    return;
		MenuOrders.showException(ex);
		return;
	    }
	    OrderDialog.this.dispose();
	}
    }

    private static final int FIELD_WIDTH = 100;

    private final boolean create;
    private Order order;
    private final Person[] persons;
    private final List<Article> articles;

    public OrderDialog(Frame parent, boolean create, Order o, Person[] persons,
	    List<Article> articles, SortedSet<Date> selection,
	    Person selectedPerson, final boolean editOrder) {
	super(parent, (create ? "Neue Bestellung" : "Bestellung bearbeiten"),
		true);
	if (!create)
	    Test.notNull(order, "can't edit order, order is null!");
	this.create = create;
	this.order = o;
	this.articles = articles;
	this.persons = persons;

	final Container contentPane = this.getContentPane();
	BorderLayout contentPaneLayout = new BorderLayout();
	contentPane.setLayout(contentPaneLayout);

	final JPanel centerPanel = new JPanel();
	centerPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
	GridBagLayout centerPanelLayout = new GridBagLayout();
	centerPanelLayout.rowWeights = new double[] { 0.1, 0.1, 0.1, 0.1 };
	centerPanelLayout.rowHeights = new int[] { 7, 7, 7, 7 };
	centerPanelLayout.columnWeights = new double[] { 0.1, 0.1 };
	centerPanelLayout.columnWidths = new int[] { 7, 7 };
	centerPanel.setLayout(centerPanelLayout);
	{
	    centerPanel.add(new JLabel("Tag"), new GridBagConstraints(0, 0, 1,
		    1, 0.0, 0.0, GridBagConstraints.WEST,
		    GridBagConstraints.NONE, new Insets(2, 2, 2, 2), 0, 0));
	}

	final JXDatePicker datePicker = new JXDatePicker();
	if (!create)
	    datePicker.setDate((Date) order.getProperty(Order.DELIVERY_DATE)
		    .getValue());
	else if (selection.size() > 0)
	    datePicker.setDate(selection.first());
	else
	    datePicker.setDate(new Date());

	final JComboBox personComboBox = new JComboBox(persons);
	if (!create) {
	    for (Person p : persons) {
		if (p.getIdentityProperty().getValue()
			.equals(order.getProperty(Order.PERSON_ID).getValue())) {
		    personComboBox.setSelectedItem(p);
		    break;
		}
	    }
	} else
	    personComboBox.setSelectedItem(selectedPerson);
	personComboBox.setPreferredSize(new Dimension(FIELD_WIDTH,
		(int) personComboBox.getPreferredSize().getHeight()));
	personComboBox.setMaximumSize(new Dimension(FIELD_WIDTH,
		(int) personComboBox.getPreferredSize().getHeight()));

	final JComboBox articleComboBox = new JComboBox(
		(DBObject[]) articles.toArray(new Article[articles.size()]));
	if (!create) {
	    for (DBObject a : articles) {
		if (a.getIdentityProperty().getValue()
			.equals(order.getProperty(Order.ARTICLE_ID).getValue())) {
		    articleComboBox.setSelectedItem(a);
		}
	    }
	} else {
	    articleComboBox.setSelectedItem(null);
	}
	articleComboBox.setMaximumSize(new Dimension(FIELD_WIDTH,
		(int) articleComboBox.getPreferredSize().getHeight()));
	articleComboBox.setPreferredSize(new Dimension(FIELD_WIDTH,
		(int) articleComboBox.getPreferredSize().getHeight()));

	final Integer value;
	if (!create) {
	    value = (Integer) order.getProperty(Order.AMOUNT).getValue();
	} else {
	    value = Integer.valueOf(1);
	}

	final JFormattedTextField amountTextField = new JFormattedTextField(
		value);
	amountTextField.setPreferredSize(new Dimension(FIELD_WIDTH,
		(int) amountTextField.getPreferredSize().getHeight()));
	amountTextField.setMaximumSize(new Dimension(FIELD_WIDTH,
		(int) amountTextField.getPreferredSize().getHeight()));
	amountTextField.setName("amountTextField");
	{
	    articleComboBox.setName("articleComboBox");
	    centerPanel.add(articleComboBox,
		    new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
			    GridBagConstraints.CENTER,
			    GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2,
				    2), 0, 0));
	}
	{
	    datePicker.setName("datePicker");
	    centerPanel.add(datePicker,
		    new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
			    GridBagConstraints.CENTER,
			    GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2,
				    2), 0, 0));
	    datePicker.setFormats(new DateFormat[] {
		    DateFormat.getDateInstance(DateFormat.MEDIUM),
		    DateFormat.getDateInstance(DateFormat.SHORT) });
	    personComboBox.setName("personComboBox");
	    centerPanel.add(personComboBox,
		    new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0,
			    GridBagConstraints.CENTER,
			    GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2,
				    2), 0, 0));
	    {
		centerPanel.add(new JLabel("Person"), new GridBagConstraints(0,
			1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
			GridBagConstraints.NONE, new Insets(2, 2, 2, 2), 0, 0));
	    }
	    {
		centerPanel.add(new JLabel("Menü"), new GridBagConstraints(0,
			2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
			GridBagConstraints.NONE, new Insets(2, 2, 2, 2), 0, 0));
	    }
	    {
		centerPanel.add(new JLabel("Anzahl"), new GridBagConstraints(0,
			3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
			GridBagConstraints.NONE, new Insets(2, 2, 2, 2), 0, 0));
	    }
	    {
		centerPanel
			.add(amountTextField, new GridBagConstraints(1, 3, 1,
				1, 0.0, 0.0, GridBagConstraints.WEST,
				GridBagConstraints.NONE,
				new Insets(2, 2, 2, 2), 20, 0));
	    }
	}

	contentPane.add(centerPanel, BorderLayout.CENTER);

	final JPanel buttons = new JPanel();
	buttons.setLayout(new BoxLayout(buttons, BoxLayout.LINE_AXIS));
	final JButton okAndAnotherButton = new JButton(new AbstractAction(
		"OK & Weitere...") {

	    {
		setEnabled(!editOrder);
	    }

	    @Override
	    public void actionPerformed(ActionEvent arg0) {
		new OrderSaverAndDialogIncrementor(amountTextField,
			articleComboBox, datePicker, personComboBox).execute();
	    }

	});
	okAndAnotherButton.setName("okAndAnotherButton");
	buttons.add(okAndAnotherButton);
	buttons.add(Box.createHorizontalGlue());
	final JButton okButton = new JButton(new AbstractAction("OK") {

	    @Override
	    public void actionPerformed(ActionEvent arg0) {
		new OrderSaver(datePicker, articleComboBox, personComboBox,
			amountTextField).execute();
	    }

	});
	okButton.setName("okButton");
	buttons.add(okButton);
	final JButton cancelButton = new JButton(new CancelAction("Abbrechen"));
	cancelButton.setName("cancelButton");
	buttons.add(cancelButton);
	contentPane.add(buttons, BorderLayout.SOUTH);
	this.pack();
	this.setLocationRelativeTo(parent);
    }

    private void saveOrder(final JXDatePicker datePicker,
	    final JComboBox personComboBox, final JComboBox articleComboBox,
	    final JFormattedTextField amountTextField)
	    throws DBManagerException {
	order.getProperty(Order.PERSON_ID).setValue(
		((Person) personComboBox.getSelectedItem())
			.getIdentityProperty().getValue());
	order.getProperty(Order.ARTICLE_ID).setValue(
		((DBObject) articleComboBox.getSelectedItem())
			.getIdentityProperty().getValue());
	order.getProperty(Order.AMOUNT).setValue(
		((Integer) amountTextField.getValue()));
	// XXX: Check if this still works! was previously set as a string!
	order.getProperty(Order.DELIVERY_DATE).setValue(datePicker.getDate());
	MenuOrders.getDBManager().save(order);
    }

}
