/*
 * Copyright (c) 2006-2011, dhiller, http://www.dhiller.de
 * Daniel Hiller, Warendorfer Str. 47, 48231 Warendorf, NRW, Germany, 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 * 
 * - Redistributions of source code must retain the above copyright notice, this 
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, 
 *   this list of conditions and the following disclaimer in the documentation 
 *   and/or other materials provided with the distribution.
 * - Neither the name of dhiller nor the names of its
 *   contributors may  
 *   be used to endorse or promote products derived from this software without 
 *   specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.dhiller.menuorders;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.dhiller.database.DBField;
import de.dhiller.database.DBManager;
import de.dhiller.database.DBObject;
import de.dhiller.database.DBProperty;
import de.dhiller.database.DBManager.DBManagerException;



/**
 * @author dhiller (creator)
 * @since 09.09.2006 15:51:39
 */
public class Person extends DBObject {

  // Declarations
  // ==================================================================================================================

  // Class ( public , package-private , protected , private - final first )
  // ----------------------------------------------------------------------------------------------
  public static final DBField< Integer >         ID   = new DBField< Integer >(
                                                            "Id" , null , Integer.class , true, 0 );
  public static final DBField< String >  NAME = new DBField< String >(
                                                            "Name" , null , String.class , false, 1 );

  private static final List< DBField< ? > > FIELDS;
  

  // Constructors
  // ==================================================================================================================
  
  static {
    List< DBField< ? > > l = new ArrayList< DBField< ? > >();
    l.add( ID );
    l.add( NAME );
    FIELDS = Collections.unmodifiableList( l );
  }

  // Public
  // ----------------------------------------------------------------------------------------------
  
  /**
   * Erzeugt eine neue <code>Person</code>-Instanz.
   */
  public Person() {
    super("Persons" );
    Set< DBProperty< ? >> props = new HashSet< DBProperty< ? >>();
    props.add( new DBProperty< Integer >( ID, null ) );
    props.add( new DBProperty< String >( NAME, null ) );
    setProperties( props );
  }
  
  /**
   * Erzeugt eine neue <code>Person</code>-Instanz.
   *
   * @param propertiesForObject
   */
  protected Person( Set< DBProperty< ? >> propertiesForObject ) {
    super("Persons" );
    setProperties( propertiesForObject );
  }

  
  // Class methods
  // ==================================================================================================================

  // Public
  // ----------------------------------------------------------------------------------------------

  public static List<Person> getInstances() throws DBManagerException {
    final ArrayList< Person > result = new ArrayList<Person>();
    Set<DBProperty<?>> filter = new HashSet<DBProperty<?>>();
    List<Set<DBProperty<?>>> properties = MenuOrders.getDBManager().getPropertySets( new Person() , filter );
    for ( Set<DBProperty<?>> propertiesForObject : properties ) {
      result.add( new Person( propertiesForObject ) );
    }
    return result;
  }


  // Instance methods
  // ==================================================================================================================

  // Public
  // ----------------------------------------------------------------------------------------------
  
  public List< DBField< ? > > getFields() {
    return FIELDS;
  }
  
  /**
   * {@inheritDoc}
   *
   * @return {@inheritDoc}
   *
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return getProperty( NAME ).getValue() != null ? ( String ) getProperty( NAME ).getValue() : super.toString();
  }

  // --------------------------------------------------------------------------
  
  @Override
  public boolean equals( Object obj ) {
    if ( this == obj )
      return true;
    if ( ! ( obj instanceof Person ) )
      return false;
    Person objID = ( Person ) obj;
    return objID.getIdentityProperty().getValue().equals( this.getIdentityProperty().getValue() );
  } // public boolean equals( Object obj )

  // --------------------------------------------------------------------------
  
  @Override
  public int hashCode() {
    int result = 17;
    result = result * 37 + ( int ) getIdentityProperty().getValue().hashCode();
    return result;
  }

}
