/*
 * Copyright (c) 2006-2011, dhiller, http://www.dhiller.de
 * Daniel Hiller, Warendorfer Str. 47, 48231 Warendorf, NRW, Germany, 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 * 
 * - Redistributions of source code must retain the above copyright notice, this 
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, 
 *   this list of conditions and the following disclaimer in the documentation 
 *   and/or other materials provided with the distribution.
 * - Neither the name of dhiller nor the names of its
 *   contributors may  
 *   be used to endorse or promote products derived from this software without 
 *   specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.dhiller.menuorders;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import de.dhiller.database.DBField;
import de.dhiller.database.DBObject;
import de.dhiller.database.DBProperty;

/**
 * @author dhiller (creator)
 * @since 10.09.2006 16:57:20
 */
public class Price extends DBObject {

    // Declarations
    // ==================================================================================================================

    // Class ( public , package-private , protected , private - final first )
    // ----------------------------------------------------------------------------------------------
    public static final DBField<Integer> ID = new DBField<Integer>("Id", null,
	    Integer.class, true, 0);
    public static final DBField<BigInteger> PRICE = new DBField<BigInteger>(
	    "Price", null, BigInteger.class, false, 1);
    public static final DBField<Integer> FROM_TOTAL_AMOUNT = new DBField<Integer>(
	    "FromTotalAmount", null, Integer.class, false, 2);
    public static final DBField<Integer> TO_TOTAL_AMOUNT = new DBField<Integer>(
	    "ToTotalAmount", null, Integer.class, false, 3);

    private static final List<DBField<?>> FIELDS;

    // Instance
    // ----------------------------------------------------------------------------------------------

    // Inner classes
    // ==================================================================================================================

    // Public
    // ----------------------------------------------------------------------------------------------

    // Constructors
    // ==================================================================================================================

    // Static
    // ----------------------------------------------------------------------------------------------

    static {
	List<DBField<?>> l = new ArrayList<DBField<?>>();
	l.add(ID);
	l.add(PRICE);
	l.add(FROM_TOTAL_AMOUNT);
	l.add(TO_TOTAL_AMOUNT);
	FIELDS = Collections.unmodifiableList(l);
    }

    // Public
    // ----------------------------------------------------------------------------------------------

    /**
     * Erzeugt eine neue <code>Price</code>-Instanz.
     * 
     */
    public Price() {
	super("Prices");
	final List<DBProperty<?>> properties = new ArrayList<DBProperty<?>>();
	for (DBField<?> f : FIELDS) {
	    properties.add(f.createProperty(null));
	}
	setProperties(new HashSet<DBProperty<?>>(properties));
    }

    // Class methods
    // ==================================================================================================================

    // Public
    // ----------------------------------------------------------------------------------------------

    // Instance methods
    // ==================================================================================================================

    // Public
    // ----------------------------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     * 
     * @return {@inheritDoc}
     * 
     * @see de.dhiller.database.DBObject#getFields()
     */
    @Override
    public List<DBField<?>> getFields() {
	return FIELDS;
    }

}
