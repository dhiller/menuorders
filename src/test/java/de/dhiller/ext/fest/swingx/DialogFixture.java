package de.dhiller.ext.fest.swingx;

import java.awt.Component;
import java.awt.Dialog;

import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.JXMonthView;

public final class DialogFixture extends
	org.fest.swing.fixture.DialogFixture {

    public DialogFixture(Dialog target) {
	super(target);
    }

    public JXDatePickerFixture datePicker(String name) {
	return new JXDatePickerFixture(robot, findComponent(JXDatePicker.class, name));
    }

    public JXMonthViewFixture monthView(String name) {
	return new JXMonthViewFixture(robot, findComponent(JXMonthView.class,
		name));
    }

    private <T extends Component> T findComponent(Class<T> t, final String name) {
	return robot.finder().findByName(name, t);
    }

}