/*
 * Copyright (c) 2011, Daniel Hiller (dhiller), Warendorfer Str. 47, 48231 Warendorf, Germany
 * http://www.dhiller.de
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 * 
 *  - 	Redistributions of source code must retain the above copyright notice, this 
 * 	list of conditions and the following disclaimer.
 *  - 	Redistributions in binary form must reproduce the above copyright notice, 
 * 	this list of conditions and the following disclaimer in the documentation 
 * 	and/or other materials provided with the distribution.
 *  - 	Neither the name of the author nor the names of its contributors may 
 * 	be used to endorse or promote products derived from this software without 
 * 	specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.dhiller.ext.fest.swingx;

import static org.testng.AssertJUnit.assertNotNull;

import java.util.Date;

import org.fest.swing.core.Robot;
import org.fest.swing.edt.GuiActionRunner;
import org.fest.swing.edt.GuiQuery;
import org.fest.swing.edt.GuiTask;
import org.fest.swing.fixture.GenericComponentFixture;
import org.jdesktop.swingx.JXDatePicker;

public final class JXDatePickerFixture extends
	GenericComponentFixture<JXDatePicker> {

    public JXDatePickerFixture(Robot robot, JXDatePicker target) {
	super(robot, target);
    }

    public Date date() {
	return GuiActionRunner.execute(new GuiQuery<Date>() {

	    @Override
	    protected Date executeInEDT() throws Throwable {
		return component().getDate();
	    }
	});
    }

    public JXDatePickerFixture requireDateNotNull() {
	assertNotNull(date());
	return this;
    }

    public void setDate(final Date newDate) {
        GuiActionRunner.execute(new GuiTask() {
    
    	@Override
    	protected void executeInEDT() throws Throwable {
    	    component().setDate(newDate);
    	}
        });
    }
}