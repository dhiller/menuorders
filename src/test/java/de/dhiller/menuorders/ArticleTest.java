/*
 * Copyright (c) 2011, Daniel Hiller (dhiller), Warendorfer Str. 47, 48231 Warendorf, Germany
 * http://www.dhiller.de
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 * 
 *  - 	Redistributions of source code must retain the above copyright notice, this 
 * 	list of conditions and the following disclaimer.
 *  - 	Redistributions in binary form must reproduce the above copyright notice, 
 * 	this list of conditions and the following disclaimer in the documentation 
 * 	and/or other materials provided with the distribution.
 *  - 	Neither the name of the author nor the names of its contributors may 
 * 	be used to endorse or promote products derived from this software without 
 * 	specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.dhiller.menuorders;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.AssertJUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.dhiller.database.DBManager.DBManagerException;
import de.dhiller.database.DBObject;
import de.dhiller.database.DBProperty;

public class ArticleTest extends DBObjectTestBase {

    private DBObject first;
    private DBObject second;

    protected Article dbObjectPrototype() {
	return new Article();
    }

    protected List<Set<DBProperty<?>>> createDBObjectProperties() {
	final List<Set<DBProperty<?>>> properties = new ArrayList<Set<DBProperty<?>>>();
	properties.add(articleProperties(1, "Artikel 1"));
	return properties;
    }

    private static Set<DBProperty<?>> articleProperties(final int id,
	    final String articleName) {
	final List<DBProperty<?>> propertiesOfFirst = Arrays
		.<DBProperty<?>> asList(Article.ID.createProperty(id),
			Article.ARTICLE_NAME.createProperty(articleName));
	final Set<DBProperty<?>> set = new HashSet<DBProperty<?>>(
		propertiesOfFirst);
	return set;
    }

    @BeforeMethod
    public void setUpTwoInstances() {
	first = newInstanceWithID(17);
	second = newInstanceWithID(42);
    }

    @Test
    public void hashCodeEqualsForNewlyCreatedInstances() {
	AssertJUnit.assertEquals(new Article().hashCode(), new Article().hashCode());
    }

    @Test
    public void hashCodeNotEqualForInstancesWithDifferentIDs() {
	AssertJUnit.assertFalse(first.hashCode() == second.hashCode());
    }

    @Test
    public void equalsObject() {
	AssertJUnit.assertTrue(new Article().equals(new Article()));
	AssertJUnit.assertTrue(first.equals(first));
	AssertJUnit.assertFalse(first.equals(second));
	AssertJUnit.assertFalse(first.equals(null));
	AssertJUnit.assertFalse(first.equals(new Object()));
    }

    @Test
    public void testArticleIdentity() {
	final DBObject article = new Article();
	AssertJUnit.assertNotNull(article.getIdentityProperty());
	AssertJUnit.assertNull("Value is " + article.getIdentityProperty().getValue(),
		article.getIdentityProperty().getValue());
    }

    @Test
    public void testGetInstances() throws DBManagerException {
	final List<Article> instances = Article.getInstances();
	AssertJUnit.assertNotNull(instances);
	AssertJUnit.assertFalse(instances.isEmpty());
    }

    @Test
    public void testGetFields() {
	final DBObject article = new Article();
	AssertJUnit.assertNotNull(article.getFields());
	AssertJUnit.assertFalse(article.getFields().isEmpty());
    }

    @Test
    public void testGetProperty() {
	AssertJUnit.assertNotNull(new Article().getProperty(Article.ID));
	AssertJUnit.assertNotNull(new Article().getProperty(Article.ARTICLE_NAME));
    }

    @Test
    public void testGetPropertiesClassOfT() {
	AssertJUnit.assertNotNull(new Article().getProperties(Article.ID.getType()));
	AssertJUnit.assertFalse(new Article().getProperties(Article.ID.getType()).isEmpty());
    }

    @Test
    public void testGetProperties() {
	AssertJUnit.assertNotNull(new Article().getProperties());
	AssertJUnit.assertFalse(new Article().getProperties().isEmpty());
    }

    @Test
    public void testWasModified() {
	AssertJUnit.assertFalse(new Article().wasModified());
	AssertJUnit.assertTrue(first.wasModified());
    }

    @Test
    public void testGetTableName() {
	AssertJUnit.assertEquals("Articles", new Article().getTableName());
    }

}
