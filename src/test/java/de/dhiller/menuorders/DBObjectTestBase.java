/*
 * Copyright (c) 2011, Daniel Hiller (dhiller), Warendorfer Str. 47, 48231 Warendorf, Germany
 * http://www.dhiller.de
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 * 
 *  - 	Redistributions of source code must retain the above copyright notice, this 
 * 	list of conditions and the following disclaimer.
 *  - 	Redistributions in binary form must reproduce the above copyright notice, 
 * 	this list of conditions and the following disclaimer in the documentation 
 * 	and/or other materials provided with the distribution.
 *  - 	Neither the name of the author nor the names of its contributors may 
 * 	be used to endorse or promote products derived from this software without 
 * 	specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.dhiller.menuorders;

import org.testng.annotations.BeforeMethod;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import de.dhiller.database.DBManager;
import de.dhiller.database.DBObject;
import de.dhiller.database.DBProperty;
import de.dhiller.database.DBManager.DBManagerException;

public abstract class DBObjectTestBase<T extends DBObject> {

    protected void createDBObjectProperties(final DBManager mockDBManager)
	    throws DBManagerException {
	final List<Set<DBProperty<?>>> properties = createDBObjectProperties();
	when(
		mockDBManager.getPropertySets(dbObjectPrototype(),
			Collections.<DBProperty<?>> emptySet())).thenReturn(
		properties);
    }

    protected abstract List<Set<DBProperty<?>>> createDBObjectProperties();

    protected T newInstanceWithID(final int id) {
	final T instance = dbObjectPrototype();
	instance.getIdentityProperty().setValue(id);
	return instance;
    }

    protected abstract T dbObjectPrototype();

    @BeforeMethod
    public void setMockDBManager() throws DBManagerException {
	final DBManager mockDBManager = mock(DBManager.class);
	createDBObjectProperties(mockDBManager);
	MenuOrders.setDbManager(mockDBManager);
    }

}
