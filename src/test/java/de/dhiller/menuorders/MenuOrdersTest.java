/*
 * Copyright (c) 2006-2011, dhiller, http://www.dhiller.de Daniel Hiller, Warendorfer Str. 47, 48231 Warendorf, NRW,
 * Germany, All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 * 
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and the following
 * disclaimer. - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution. - Neither the
 * name of dhiller nor the names of its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.dhiller.menuorders;

import static org.mockito.Mockito.*;
import static de.dhiller.menuorders.MenuOrders.ComponentNames.*;
import static org.testng.AssertJUnit.assertNotNull;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

import org.fest.swing.edt.GuiActionRunner;
import org.fest.swing.edt.GuiTask;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import de.dhiller.database.DBManager;
import de.dhiller.database.DBManager.ConnectionProperties;
import de.dhiller.database.DBManager.DBManagerException;
import de.dhiller.ext.fest.swingx.FrameFixture;

public class MenuOrdersTest {

    public static abstract class BaseTestMenuOrders extends
	    AbstractTestWithMockDBManager {

	private FrameFixture menuOrders;

	@BeforeClass
	public void setUpFrame() throws SQLException {
	    final Connection mockConnection = mock(Connection.class);
	    final Statement mockStatement = mock(Statement.class);
	    final ResultSet mockResultSet = mock(ResultSet.class);
	    when(mockStatement.executeQuery(anyString())).thenReturn(
		    mockResultSet);
	    when(mockConnection.createStatement()).thenReturn(mockStatement);
	    when(dbManager.getConnection()).thenReturn(mockConnection);

	    GuiActionRunner.execute(new GuiTask() {

		@Override
		protected void executeInEDT() throws Throwable {
		    final MenuOrders menuOrders = new MenuOrders();
		    menuOrders.setVisible(true);
		}
	    });
	    setMenuOrders(new FrameFixture(FRAME));
	}

	@AfterClass
	public void cleanUp() {
	    menuOrders().cleanUp();
	}

	protected FrameFixture menuOrders() {
	    return menuOrders;
	}

	private void setMenuOrders(FrameFixture menuOrders) {
	    this.menuOrders = menuOrders;
	}

	protected void tabOrdersByPersonPerMonth() {
	    menuOrders().tabbedPane(TABBED_PANE).selectTab(3);
	}

	protected void tabOrdersByPersonAndDay() {
	    menuOrders().tabbedPane(TABBED_PANE).selectTab(2);
	}

	protected void tabAllOrdersForWeek() {
	    menuOrders().tabbedPane(TABBED_PANE).selectTab(1);
	}

	protected void tabOrdersForPersonAndWeek() {
	    menuOrders().tabbedPane(TABBED_PANE).selectTab(0);
	}

    }

    @Test(groups = { "gui" })
    public static class Basics extends BaseTestMenuOrders {

	@Test
	public void hasTabbedPane() {
	    assertNotNull(menuOrders().tabbedPane(TABBED_PANE));
	}

	@Test
	public void hasPersonList() {
	    assertNotNull(menuOrders().list(PERSON_LIST));
	}

	@Test
	public void hasMonthView() {
	    assertNotNull(menuOrders().monthView(MONTH_VIEW));
	}

    }

    @Test(groups = { "gui" })
    public static class OrdersForPersonAndWeek extends BaseTestMenuOrders {

	@BeforeClass
	public void selectTab() {
	    tabOrdersForPersonAndWeek();
	}

	@Test
	public void hasOrdersForPersonAndWeek() {
	    assertNotNull(menuOrders().panel(ORDERS_FOR_ONE_PERSON_AND_WEEK));
	}

	@Test
	public void hasMarkPaidUnpaid() {
	    assertNotNull(menuOrders().panel(TOGGLE_PAID));
	}

	@Test
	public void hasPersonAndWeek() {
	    assertNotNull(menuOrders().table(
		    ORDERS_FOR_ONE_PERSON_AND_WEEK_TABLE));
	}

	@Test
	public void hasTogglePaidUnpaid() {
	    assertNotNull(menuOrders().checkBox(TOGGLE_PAID_UNPAID));
	}

	@Test
	public void hasCreateNewOrder() {
	    assertNotNull(menuOrders().button(CREATE_NEW_ORDER));
	}

    }

    @Test(groups = { "gui" })
    public static class AllOrdersForWeek extends BaseTestMenuOrders {

	@BeforeClass
	public void selectTab() {
	    tabAllOrdersForWeek();
	}

	@Test
	public void hasPanel() {
	    assertNotNull(menuOrders().panel(ALL_ORDERS_FOR_WEEK));
	}

	@Test
	public void hasTable() {
	    assertNotNull(menuOrders().table(ALL_ORDERS_FOR_WEEK_TABLE));
	}

	@Test
	public void hasLabels() {
	    assertNotNull(menuOrders().label(ALL_ORDERS_PAID));
	    assertNotNull(menuOrders().label(ALL_ORDERS_REMAINING));
	    assertNotNull(menuOrders().label(ALL_ORDERS_TOTAL));
	}

	@Test
	public void hasTogglePlacedOrder() {
	    assertNotNull(menuOrders().checkBox(TOGGLE_ORDER_PLACED));
	}

    }

    @Test(groups = { "gui" })
    public static class OrdersByPersonAndDay extends BaseTestMenuOrders {

	@BeforeClass
	public void selectTab() {
	    tabOrdersByPersonAndDay();
	}

	@Test
	public void hasOrdersByPersonAndDay() {
	    assertNotNull(menuOrders().panel(ORDERS_BY_PERSON_AND_DAY));
	}

	@Test
	public void hasOrdersByPersonAndDayTable() {
	    assertNotNull(menuOrders().table(ORDERS_BY_PERSON_AND_DAY_TABLE));
	}

	@Test
	public void hasPrintButtons() {
	    assertNotNull(menuOrders().button(PRINT_BUTTON));
	}

    }

    @Test(groups = { "gui" })
    public static class OrdersByPersonPerMonth extends BaseTestMenuOrders {

	@BeforeClass
	public void selectTab() {
	    tabOrdersByPersonPerMonth();
	}

	@Test
	public void hasOrdersByPersonPerMonth() {
	    assertNotNull(menuOrders().panel(ORDERS_BY_PERSON_PER_MONTH));
	}

	@Test
	public void hasMonthSpinner() {
	    assertNotNull(menuOrders().spinner(MONTH_SPINNER));
	}

	@Test
	public void hasYearSpinner() {
	    assertNotNull(menuOrders().spinner(YEAR_SPINNER));
	}

    }

    public static abstract class MemDBTestMenuOrders {

	@BeforeClass
	public void setUpMemDB() throws DBManagerException, IOException,
		SQLException {
	    final DBManager dbManager = new DBManager(
		    DBManager.newMemoryConnectionProperties());
	    executeBatchesFromFile(dbManager, new File("db_definition.txt"));
	    executeBatchesFromFile(dbManager, new File("default_data.txt"));
	}

	private void executeBatchesFromFile(final DBManager dbManager,
		final File sqlFile) throws FileNotFoundException, IOException,
		SQLException {
	    final Connection connection = dbManager.getConnection();
	    final BufferedReader r = new BufferedReader(new FileReader(sqlFile));
	    StringBuilder sql = new StringBuilder();
	    String line = null;
	    try {
		while ((line = r.readLine()) != null) {
		    if (line.matches("\\s*(SHUTDOWN COMPACT;|//.*)?\\s*")) {
			continue;
		    }
		    if (line.matches("\\s*INSERT INTO.*;\\s*")) {
			executeBatch(connection, line);
			continue;
		    }
		    if (line.matches("\\s*GO;\\s*")) {
			executeBatch(connection, sql.toString());
			sql = new StringBuilder();
			continue;
		    }
		    sql.append(line + "\n");
		}
	    } finally {
		r.close();
	    }
	    executeBatch(connection, sql.toString());
	}

	private void executeBatch(final Connection connection, final String sql)
		throws SQLException {
	    final PreparedStatement prepareStatement = connection
		    .prepareStatement(sql);
	    try {
		Logger.getLogger(getClass().getCanonicalName()).info(
			"Executing batch:\n" + sql);
		prepareStatement.executeBatch();
	    } finally {
		prepareStatement.close();
	    }
	}

    }

    public static class TestAllOrdersForPerson extends MemDBTestMenuOrders {

	@Test
	public void testBlah() throws Exception {
	    Assert.fail("TODO"); // TODO: Test implementieren!
	}
    }

}
