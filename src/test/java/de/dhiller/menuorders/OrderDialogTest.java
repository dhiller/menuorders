/*
 * Copyright (c) 2011, Daniel Hiller (dhiller), Warendorfer Str. 47, 48231 Warendorf, Germany
 * http://www.dhiller.de
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 * 
 *  - 	Redistributions of source code must retain the above copyright notice, this 
 * 	list of conditions and the following disclaimer.
 *  - 	Redistributions in binary form must reproduce the above copyright notice, 
 * 	this list of conditions and the following disclaimer in the documentation 
 * 	and/or other materials provided with the distribution.
 *  - 	Neither the name of the author nor the names of its contributors may 
 * 	be used to endorse or promote products derived from this software without 
 * 	specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.dhiller.menuorders;

import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

import org.mockito.ArgumentCaptor;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TreeSet;

import org.fest.swing.fixture.JButtonFixture;
import org.fest.swing.fixture.JComboBoxFixture;
import org.fest.swing.fixture.JTextComponentFixture;

import de.dhiller.database.DBManager.DBManagerException;
import de.dhiller.database.DBObject;
import de.dhiller.ext.fest.swingx.JXDatePickerFixture;
import de.dhiller.ext.fest.swingx.DialogFixture;

public class OrderDialogTest extends AbstractTestWithMockDBManager {

    private static final int SECOND_PERSON_ID = 66;

    private static final int FIRST_PERSON_ID = 37;

    private static final int SECOND_ARTICLE_ID = 42;

    private static final int FIRST_ARTICLE_ID = 17;

    protected DialogFixture dialogFixture;
    protected JComboBoxFixture personComboBox;
    protected JXDatePickerFixture datePicker;
    protected JComboBoxFixture articleComboBox;
    protected JButtonFixture okAndAnotherButton;
    protected JButtonFixture okButton;
    protected JButtonFixture cancelButton;
    protected JTextComponentFixture amountTextField;

    @Test(groups = { "gui" })
    public static class Basics extends OrderDialogTest {

	@BeforeClass
	public void setupDialog() throws Exception {
	    setUpDialogForCreate();
	}

	@AfterClass
	public void cleanUp() {
	    cleanupDialog();
	}

	@Test
	public void enabled() throws Exception {
	    articleComboBox.requireEnabled();
	    personComboBox.requireEnabled();
	    datePicker.requireEnabled();
	    okButton.requireEnabled();
	    okAndAnotherButton.requireEnabled();
	    cancelButton.requireEnabled();
	}

	@Test
	public void elementsHaveSelection() throws Exception {
	    articleComboBox.requireItemCount(2);
	    personComboBox.requireItemCount(2);
	    datePicker.requireDateNotNull();
	}

    }

    @Test(groups = { "gui" })
    public static class Failures extends OrderDialogTest {

	@AfterMethod(alwaysRun = true)
	public void cleanUp() {
	    cleanupDialog();
	}

	@Test(description = "Das sollte eigentlich funktionieren!")
	public void tryToCreateOrderWithOrderNull() throws Exception {
	    setUpDialogFixture(persons(), articles(), null);
	    selectFirstArticle();
	    selectFirstPerson();
	    okButton.click();
	    verifyOrderNotSaved();
	}

	@Test(expectedExceptions = NullPointerException.class)
	public void editOrderButOrderIsNull() throws Exception {
	    setUpDialogForCreate();
	    new OrderDialog(null, false, null, new Person[0],
		    Arrays.asList(new Article()), new TreeSet<Date>(
			    Collections.<Date> emptySet()), null, false);
	}

    }

    @Test(groups = { "gui" })
    public static class SaveCreatedOrder extends OrderDialogTest {

	private AssertSavedOrder savedOrder;

	@BeforeClass
	public void createOrderWithDialog() throws Exception {
	    createDefaultOrder();
	    savedOrder = new AssertSavedOrder(this.savedOrder());
	}

	@AfterClass
	public void cleanUp() {
	    cleanupDialog();
	}

	@Test
	public void orderSaved() throws Exception {
	    verifyOrderSaved();
	}

	@Test
	public void savedOrderHasSelectedDefaultDate() throws Exception {
	    savedOrder.assertDateIs(daysFromNow(0));
	}

	@Test
	public void savedOrderHasSelectedPerson() throws Exception {
	    savedOrder.assertPersonIDIs(FIRST_PERSON_ID);
	}

	@Test
	public void savedOrderHasSelectedArticle() throws Exception {
	    savedOrder.assertArticleIDIs(FIRST_ARTICLE_ID);
	}

	@Test
	public void quantityIsOne() throws Exception {
	    savedOrder.assertQuantityIs(1);
	}

    }

    @Test(groups = { "gui" })
    public static class SaveEditedOrder extends OrderDialogTest {

	private static final int EXPECTED_CHANGED_QUANTITY = 2;
	private Date expectedDate;
	private AssertSavedOrder savedOrder;

	@BeforeClass
	public void changeCreatedOrder() throws Exception {
	    createDefaultOrder();
	    cleanupDialog();
	    setUpDialogFixture(persons(), articles(), savedOrder());
	    articleComboBox.selectItem(1);
	    personComboBox.selectItem(1);
	    expectedDate = daysFromNow(1);
	    datePicker.setDate(expectedDate);
	    amountTextField.setText(String.valueOf(EXPECTED_CHANGED_QUANTITY));
	    okButton.click();
	    savedOrder = new AssertSavedOrder(this.savedOrder());
	}

	@Test
	public void personChanged() throws Exception {
	    savedOrder.assertPersonIDIs(SECOND_PERSON_ID);
	}

	@Test
	public void articleChanged() throws Exception {
	    savedOrder.assertArticleIDIs(SECOND_ARTICLE_ID);
	}

	@Test
	public void dateChanged() throws Exception {
	    savedOrder.assertDateIs(expectedDate);
	}

	@Test
	public void quantityChanged() throws Exception {
	    savedOrder.assertQuantityIs(EXPECTED_CHANGED_QUANTITY);
	}

    }

    protected List<Article> articles() {
	final Article first = article(FIRST_ARTICLE_ID);
	final Article second = article(SECOND_ARTICLE_ID);
	return Arrays.asList(first, second);
    }

    protected Person[] persons() {
	return new Person[] { person(FIRST_PERSON_ID),
		person(SECOND_PERSON_ID), };
    }

    protected Person person(int id) {
	return setID(new Person(), id);
    }

    protected Article article(int id) {
	return setID(new Article(), id);
    }

    private <T extends DBObject> T setID(final T person, int id) {
	person.getIdentityProperty().setValue(id);
	return person;
    }

    protected void createDefaultOrder() throws Exception {
	setUpDialogForCreate();
	selectFirstArticle();
	selectFirstPerson();
	okButton.click();
    }

    protected void selectFirstPerson() {
	personComboBox.selectItem(0);
    }

    protected void selectFirstArticle() {
	articleComboBox.selectItem(0);
    }

    protected void verifyOrderSaved() throws DBManagerException {
	verify(dbManager).save(any(Order.class));
    }

    protected void verifyOrderNotSaved() throws DBManagerException {
	verify(dbManager, times(0)).save(any(Order.class));
    }

    protected void setUpDialogForCreate() throws Exception {
	setUpDialogFixture(persons(), articles(), new Order());
    }

    protected void setUpDialogFixture(final Person[] persons,
	    final List<Article> articles, final Order order) throws Exception {
	if (this.dialogFixture != null)
	    this.dialogFixture.cleanUp();
	final DialogFixture dialogFixture = new DialogFixture(
		new OrderDialogCreator(persons, articles, order).create());
	dialogFixture.show();
	articleComboBox = dialogFixture.comboBox("articleComboBox");
	personComboBox = dialogFixture.comboBox("personComboBox");
	okAndAnotherButton = dialogFixture.button("okAndAnotherButton");
	okButton = dialogFixture.button("okButton");
	cancelButton = dialogFixture.button("cancelButton");
	datePicker = dialogFixture.datePicker("datePicker");
	amountTextField = dialogFixture.textBox("amountTextField");
	this.dialogFixture = dialogFixture;
    }

    protected void cleanupDialog() {
	if (dialogFixture != null)
	    dialogFixture.cleanUp();
	dialogFixture = null;
    }

    protected Order savedOrder() throws DBManagerException {
	final ArgumentCaptor<Order> captor = ArgumentCaptor
		.forClass(Order.class);
	verify(dbManager, atLeastOnce()).save(captor.capture());
	final Order savedOrder = captor.getValue();
	return savedOrder;
    }

    protected Date daysFromNow(int daysFromNow) {
	final Calendar instance = Calendar.getInstance();
	instance.set(Calendar.HOUR_OF_DAY, 0);
	instance.set(Calendar.MINUTE, 0);
	instance.set(Calendar.SECOND, 0);
	instance.set(Calendar.MILLISECOND, 0);
	instance.add(Calendar.DAY_OF_MONTH, daysFromNow);
	final Date expectedDate = instance.getTime();
	return expectedDate;
    }

}
