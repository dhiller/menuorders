/*
 * Copyright (c) 2011, Daniel Hiller (dhiller), Warendorfer Str. 47, 48231 Warendorf, Germany
 * http://www.dhiller.de
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 * 
 *  - 	Redistributions of source code must retain the above copyright notice, this 
 * 	list of conditions and the following disclaimer.
 *  - 	Redistributions in binary form must reproduce the above copyright notice, 
 * 	this list of conditions and the following disclaimer in the documentation 
 * 	and/or other materials provided with the distribution.
 *  - 	Neither the name of the author nor the names of its contributors may 
 * 	be used to endorse or promote products derived from this software without 
 * 	specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.dhiller.menuorders;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.dhiller.database.DBField;
import de.dhiller.database.DBObject;
import de.dhiller.database.DBProperty;

public class OrderTest extends DBObjectTestBase {

    @Test
    public void createNewOrder() {
	newOrder();
    }

    @Test
    public void hasFields() throws Exception {
	AssertJUnit.assertNotNull(newOrder().getFields());
	AssertJUnit.assertFalse(newOrder().getFields().isEmpty());
    }

    @Test
    public void hasIdentity() throws Exception {
	AssertJUnit.assertNotNull(newOrder().getIdentityProperty());
    }

    @Test
    public void hasAllProperties() throws Exception {
	final Order newOrder = newOrder();
	for (DBField<?> f : newOrder.getFields())
	    AssertJUnit.assertNotNull(newOrder.getProperty(f));
    }

    @Test
    public void checkEquals() throws Exception {
	AssertJUnit.assertTrue(newOrder().equals(newOrder()));
	final Order first = newOrderWithIdentity(17);
	AssertJUnit.assertTrue(first.equals(first));
	AssertJUnit.assertTrue(first.equals(newOrderWithIdentity(17)));
	final Order second = newOrderWithIdentity(42);
	AssertJUnit.assertFalse(first.equals(second));
    }

    @Test
    public void checkHashcode() throws Exception {
	AssertJUnit.assertEquals(newOrder().hashCode(), newOrder().hashCode());
	final Order first = newOrderWithIdentity(17);
	AssertJUnit.assertEquals(first.hashCode(), newOrderWithIdentity(17).hashCode());
	final Order second = newOrderWithIdentity(42);
	AssertJUnit.assertFalse(first.hashCode() == second.hashCode());
    }

    @Test
    public void testGetInstances() throws Exception {
	AssertJUnit.assertNotNull(Order.getInstances());
	AssertJUnit.assertFalse(Order.getInstances().isEmpty());
    }

    private Order newOrderWithIdentity(final int id) {
	final Order first = newOrder();
	first.getIdentityProperty().setValue(id);
	return first;
    }

    private Order newOrder() {
	return new Order();
    }

    @Override
    protected List<Set<DBProperty<?>>> createDBObjectProperties() {
	final List<Set<DBProperty<?>>> properties = new ArrayList<Set<DBProperty<?>>>();
	final Set<DBProperty<?>> propertiesForObject = new HashSet<DBProperty<?>>();
	propertiesForObject.add(Order.ID.createProperty(17));
	propertiesForObject.add(Order.AMOUNT.createProperty(2));
	propertiesForObject.add(Order.ARTICLE_ID.createProperty(1));
	propertiesForObject.add(Order.DELIVERY_DATE
		.createProperty("2011-01-01"));
	propertiesForObject.add(Order.PAID.createProperty(false));
	propertiesForObject.add(Order.PERSON_ID.createProperty(1));
	propertiesForObject.add(Order.PLACED.createProperty(true));
	properties.add(propertiesForObject);
	return properties;
    }

    @Override
    protected DBObject dbObjectPrototype() {
	return new Order();
    }

}
