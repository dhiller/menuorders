/*
 * Copyright (c) 2006-2011, dhiller, http://www.dhiller.de Daniel Hiller, Warendorfer Str. 47, 48231 Warendorf, NRW,
 * Germany, All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 * 
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and the following
 * disclaimer. - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution. - Neither the
 * name of dhiller nor the names of its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package de.dhiller.menuorders;

import static org.testng.AssertJUnit.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.AssertJUnit;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import de.dhiller.database.DBField;
import de.dhiller.database.DBManager;
import de.dhiller.database.DBManager.DBManagerException;
import de.dhiller.database.DBObject;
import de.dhiller.database.DBProperty;


/**
 * @author dhiller (creator)
 * @since 09.09.2006 17:18:10
 */
public class PersonTest {

  public static class PersonTestBase {

    protected DBManager dbManager;

    @BeforeMethod
    public void setUp() throws java.lang.Exception {
      dbManager = newTestDBInstance();
      MenuOrders.setDbManager( dbManager );
    }

    @AfterMethod
    public void tearDown() throws java.lang.Exception {
      dbManager.cleanUp();
    }

  }

  public static final class PersonProperties extends PersonTestBase {

    @Test
    public void onCreationNotModified() throws Exception {
      AssertJUnit.assertFalse( new Person().wasModified() );
    }

    @Test
    public void hasTwoProperties() throws Exception {
      AssertJUnit.assertEquals( 2 , new Person().getProperties().size() );
    }

    @Test
    public void hasName() throws Exception {
      AssertJUnit.assertNotNull( new Person().getProperty( Person.NAME ) );
    }

    @Test
    public void hasID() throws Exception {
      AssertJUnit.assertNotNull( new Person().getProperty( Person.ID ) );
    }

    @Test
    public void noIDValue() throws Exception {
      assertNull( new Person().getProperty( Person.ID ).getValue() );
    }

    @Test
    public void nameModified() throws Exception {
      final DBProperty< String > property = new Person().getProperty( Person.NAME );
      property.setValue( "Heinz" );
      AssertJUnit.assertTrue( property.wasModified() );
    }

    @Test
    public void instanceModified() throws Exception {
      Person testInstance = new Person();
      final DBProperty< String > property = testInstance.getProperty( Person.NAME );
      property.setValue( "Heinz" );
      AssertJUnit.assertTrue( testInstance.wasModified() );
    }

  }

  public static final class Database extends PersonTestBase {

    private Statement statement;
    private Person    heinz;

    @BeforeMethod
    public void setUpStatement() throws SQLException {
      statement = dbManager.getConnection().createStatement();
    }

    @AfterMethod
    public void cleanUpDatabase() throws SQLException {
      statement.executeUpdate( "DELETE FROM Persons WHERE " + Person.NAME.getName() + "='Heinz'" );
      statement.close();
    }

    @Test
    public void testHasPersons() throws Exception {
      AssertJUnit.assertFalse( Person.getInstances().isEmpty() );
    }

    @Test
    public void testHasIDAfterSave() throws Exception {
      heinz = saveHeinz();
      assertHasValue( Person.ID , heinz );
    }

    @Test
    public void testNotModifiedAfterSave() throws Exception {
      heinz = saveHeinz();
      assertNotModified( heinz );
    }

    @Test
    public void countIsGreater() throws Exception {
      int oldCount = count( statement , "Persons" );
      heinz = saveHeinz();
      AssertJUnit.assertEquals( oldCount + 1 , count( statement , "Persons" ) );
    }

    @Test
    public void getInstancesGrown() throws Exception {
      int oldCount = count( statement , "Persons" );
      heinz = saveHeinz();
      AssertJUnit.assertEquals( oldCount + 1 , Person.getInstances().size() );
    }

    @Test
    public void canRetrieveSavedInstance() throws Exception {
      heinz = saveHeinz();
      final Person createdPerson = Person.getInstances().get( Person.getInstances().size() - 1 );
      assertHasValue( Person.ID , heinz );
      AssertJUnit.assertEquals( "Heinz" , createdPerson.getProperty( Person.NAME ).getValue() );
      AssertJUnit.assertEquals( Person.getInstances().get( Person.getInstances().size() - 1 ) , heinz );
    }

    @Test
    public void testDeleteInstance() throws Exception {
      int oldCount = count( statement , "Persons" );
      heinz = saveHeinz();
      dbManager.delete( ( DBObject ) heinz );
      AssertJUnit.assertEquals( oldCount , Person.getInstances().size() );
    }

    private void assertNotModified( Person object ) {
      AssertJUnit.assertFalse( object.wasModified() );
    }

    private void assertHasValue( DBField< Integer > field , Person dbObject ) {
      assertNotNull( dbObject.getProperty( field ).getValue() );
    }


    private Person saveHeinz() throws DBManagerException {
      final Person testInstance = new Person();
      ( ( DBProperty< String > ) testInstance.getProperty( Person.NAME ) ).setValue( "Heinz" );
      dbManager.save( testInstance );
      return testInstance;
    }

    private void printPersons( final Statement s ) throws SQLException {
      final ResultSet persons = s.executeQuery( "SELECT * FROM Persons" );
      try {
        while ( persons.next() ) {
		    System.out
			    .println("executeQuery.getInt( 1 ): |" + persons.getInt(1) + "|"); //$NON-NLS-1$ //$NON-NLS-2$  
		    System.out
			    .println("executeQuery.getString( 2 ): |" + persons.getString(2) + "|"); //$NON-NLS-1$ //$NON-NLS-2$
        }
      }
      finally {
        persons.close();
      }
    }

    private int count( final Statement s , String tableName ) throws SQLException {
      final int count;
      final ResultSet executeQuery = s.executeQuery( String.format( "SELECT COUNT(*) FROM %s" , tableName ) );
      try {
        AssertJUnit.assertTrue( executeQuery.next() );
        count = executeQuery.getInt( 1 );
      }
      finally {
        executeQuery.close();
      }
      return count;
    }

  }

  private static DBManager newTestDBInstance() throws DBManagerException , SQLException , IOException {
    return new DBManager( DBManager.newFileConnectionProperties( MenuOrders.FILE_NAME , "sa" , "" ) );
  }

}
